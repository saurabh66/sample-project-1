<?php

namespace Classes\WHMCS;

/**
 * Class CallWHMCS
 * API Class to connect with Remote WHCMS App
 * @package Classes\WHMCS
 */
class CallWHMCS
{
    /**
     * URL to WHMCS API file goes here
     * @var string
     */
    private $whmcsUrl = 'https://192.168.0.5/';
    /**
     * Admin username goes here
     * @var string
     */
    private $username = 'xxxxxxxx';
    /**
     * # Admin password goes here
     * @var string
     */
    private $password = 'xxxxxxxx';
    /**
     * Log retention for days
     * @var int
     */
    private $logdays = 60;

    /**
     * @param string $action
     * @param array $data
     * @param string $type
     * @return bool|mixed
     */
    function getResponse($action="", $data=[] , $type="json")
    {

        /**
         * Prepare Post data
         */
        $postfields = array();
        $postfields["username"] = $this->username;
        $postfields["password"] = md5($this->password);
        $postfields["action"] = $action;

        foreach($data as $key=>$value) {
            $postfields[$key]=$value;
        }

        $postfields["responsetype"] = $type;

        $url = $this->whmcsUrl.'includes/api.php';
        $query_string = "";
        foreach ($postfields AS $k=>$v) $query_string .= "$k=".urlencode($v)."&";

        /**
         * Initiate cURL Request
         */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $json = curl_exec($ch);

        if (curl_error($ch) || !$json) {
            curl_close($ch);
            return false;
        } else {
            return $json;
        }
    }
}