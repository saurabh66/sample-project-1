<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(

    'service_manager' => [
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ],
    ],
    'db' => [
        // root adapter called from $services->get(\Zend\Db\Adapter\AdapterInterface::class);
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=pportal;host=192.168.0.4',
        'driver_options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ],

        // more adapters
        'adapters' => [
            'db1' => [
                'driver' => 'Pdo',
                'dsn' => 'mysql:dbname=pportal;host=192.168.0.4',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ],
            ],
            'db2' => [
                'driver' => 'Pdo',
                'dsn' => 'mysql:dbname=freeradiusnew;host=192.168.0.4',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ],
            ],
        ],
    ],
);