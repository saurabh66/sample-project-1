<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action' => 'index',
                    ],
                ],
            ],

            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'login',
                    ],
                ],
            ],
            'log-retaintion' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/log-retaintion',
                    'defaults' => [

                        'controller' => Controller\CronController::class,
                        'action' => 'logRetaintion',
                    ],
                ],
            ],
            'advance' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/advance',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'advance',
                    ],
                ],
            ],
            'advancesubmit' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/advancesubmit',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'advancesubmit',
                    ],
                ],
            ],

            'parseusers' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/parseusers',
                    'defaults' => [

                        'controller' => Controller\CronController::class,
                        'action' => 'parseusers',
                    ],
                ],
            ],


            'report' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/report',
                    'defaults' => [

                        'controller' => Controller\ReportController::class,
                        'action' => 'report',
                    ],
                ],
            ],

            'support' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/support',
                    'defaults' => [

                        'controller' => Controller\SupportController::class,
                        'action' => 'support',
                    ],
                ],
            ],

            'supportemail' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/supportemail',
                    'defaults' => [

                        'controller' => Controller\SupportController::class,
                        'action' => 'supportemail',
                    ],
                ],
            ],

            'summaryreport' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/summaryreport',
                    'defaults' => [

                        'controller' => Controller\ReportController::class,
                        'action' => 'summaryreport',
                    ],
                ],
            ],

            'signup' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/signup',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'signup',
                    ],
                ],
            ],
            'editprofile' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/editprofile',
                    'defaults' => [

                        'controller' => Controller\UserController::class,
                        'action' => 'editprofile',
                    ],
                ],
            ],


            'skip2fa' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/skip2fa',
                    'defaults' => [

                        'controller' => Controller\UserController::class,
                        'action' => 'skip2fa',
                    ],
                ],
            ], 'otp' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/otp',
                    'defaults' => [

                        'controller' => Controller\UserController::class,
                        'action' => 'otp',
                    ],
                ],
            ],

            'set2fa' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/set2fa',
                    'defaults' => [

                        'controller' => Controller\UserController::class,
                        'action' => 'set2fa',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'logout',
                    ],
                ],
            ],

            'dashboard' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/dashboard',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'contactsubmit' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/contactsubmit',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'contactsubmit',
                    ],
                ],
            ],


            'activate-a-device' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/activate-a-device',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'activatedevice',
                    ],
                ],
            ],
            'reset-password' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/reset-password',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'resetpassword',
                    ],
                ],
            ],
            'forgetpassword' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/forgetpassword',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'forgetpassword',
                    ],
                ],
            ],
            'signupsubmit' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/signupsubmit',
                    'defaults' => [

                        'controller' => Controller\IndexController::class,
                        'action' => 'signupsubmit',
                    ],
                ],
            ],

            'activation' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/activation',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'activation',
                    ],
                ],
            ],

            'activationrequest' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/activationrequest',
                    'defaults' => [

                        'controller' => Controller\DashboardController::class,
                        'action' => 'activationrequest',
                    ],
                ],
            ],
            'requestDedicatedIP' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/request_dedicated_ip',
                    'defaults' => [
                        'controller' => Controller\GroupApiController::class,
                        'action' => 'requestDedicatedIP',
                    ],
                ],
            ],
            'application' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'team-member' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/team-member[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\TeamMemberController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'support' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/support[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\SupportController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'groups' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/groups[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\GroupController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'editgroup' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/editgroup[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\GroupController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'editgroup' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/editgroup[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\GroupController::class,
                        'action' => 'editgroup',
                    ],
                ],
            ],
            'updateinfo' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/updateinfo',
                    'defaults' => [
                        'controller' => Controller\GroupController::class,
                        'action' => 'updateinfo',
                    ],
                ],
            ],

            'get-tickets-view' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/users/get-tickets',
                    'defaults' => [

                        'controller' => Controller\ApiController::class,
                        'action' => 'getTickets',
                    ],
                ],
            ],

            'ticket-details' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/users/ticket-details',
                    'defaults'=>[
                        'controller' => Controller\ApiController::class,
                        'action' => 'ticketDetails',
                    ],
                ],
                // 'may_terminate' => true,

            ],
            'add-ticket' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/users/add-ticket',
                    'defaults'=>[
                        'controller' => Controller\ApiController::class,
                        'action' => 'addTicket',
                    ],
                ],
                // 'may_terminate' => true,

            ],
            'users/post-ticket-reply' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/users/post-ticket-reply',
                    'defaults' => [

                        'controller' => Controller\ApiController::class,
                        'action' => 'postTicketReply',
                    ],
                ],
            ],
            'api' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api[/:action[/[:id]]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\ApiController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'login-as-customer' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/login-as-customer[/:admin_id][/:userid]',
                    'constraints' => array(
                        'admin_id' => '[0-9]+',
                        'userid' => '[0-9]+',
                    ),
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'loginAsCustomer',
                    ],
                ],
            ],
        'test_automated_job' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/test_automated_job',
                'defaults' => [
                    'controller' => Controller\CronController::class,
                    'action' => 'pingDedicatedServers',
                ],
            ],
        ],
        ],
],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],

];
