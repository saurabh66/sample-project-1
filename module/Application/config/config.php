<?php
return [

    /**
     * Amazon SES Configs
     */
    "HOSTNAME" => "email-smtp.us-west-2.amazonaws.com",
    "USERNAME" => "xxxxxxxxxx",
    "PASSWORD" => "xxxxxxxxxx",
    "PORT" => 587,
    "SSL" => "tls",
    'fromName' => 'Appstractor | App',
    'fromEmail' => 'no-reply@App.com',
    'contactEmail'=>'developer@example.com.com',
    'whmcs_api' => [
        'url' => 'https://192.168.0.5/',
        'user' => 'PUPortal',
        'password' => 'xxxxxxxxxx',
        'productid' => 1,
        'productname' => 'App',
        'logdays' => 60
    ],

    /**
     * Mobile API activation expiry confis
     */
    'activationExpiry' => [
        'selfInvite' => 172800,
        'teamInvite' => 172800
    ],

    /**
     * Emails subjects
     */
    'EmailSubject'=>[
        'selfInvite'=>'Invitation to App',
        'bultInvite'=>'Install the  App on Your Device',
        'resetPassword'=>'Password Reset Link',
        'signup'=>'Welcome to App',
        'ApiSendLink'=>'App Installer Link',
        'NotifyAdminForExpiredInvitation'=>'Resend Activation Link',
        'AccessLink'=>"Account Access Link",
        'ContactUS'=>"Contact To Admin",
        'ContactUsUser'=>"Reply To Admin",
        'ActivateAdminNofify'=>"Activation request "

    ],

    /**
     * Mobile API key for FDC notification
     */
    'MobileKey'=>'xxxxxxxxxxxxxxxxxxxxxxxxxxxx',

    /**
     * Dashboard Widget and Reports configs
     */
    'dashboard'=>[
        "users"=>[
            "recent"=>3,
            "active_last_start"=>24,
            "active_last_end"=>(24*7),
            "inactive"=>(24*7)

        ],
        "device"=>[
            "recent"=>3,
            "active_last_start"=>24,
            "active_last_end"=>(24*7),
            "inactive"=>(24*7)
        ]
    ],

    /**
     * Password configs for clients
     */
    'password'=>[
        "minlength" => 8,
        "maxlength" => 15
        ]
];

?>