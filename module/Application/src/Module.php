<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;


use Application\Model\Countries;
use Application\Model\CountriesTable;
use Application\Model\ProductUsers;
use Application\Model\ProductUsersTable;
use Application\Model\ProductUsersProfile;
use Application\Model\ProductUsersProfileTable;
use Application\Model\GroupLocationTable;
use Application\Model\GroupLocation;
use Application\Model\Locations;
use Application\Model\LocationsTable;
use Application\Model\Nas;
use Application\Model\NasTable;
use Application\Model\RadAcct;
use Application\Model\RadAcctTable;
use Application\Model\ProcessUsername;
use Application\Model\UserDevicesTable;
use Application\Model\UserDevices;
use Application\Model\ProcessUsernameTable;
use Application\Model\CountryCoords;
use Application\Model\CountryCoordsTable;
use Application\Model\Hash;
use Application\Model\RadCheck;
use Application\Model\RadCheckTable;
use Application\Model\UserActivationRequest;
use Application\Model\UserActivationRequestTable;
use Application\Model\HashTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Application\Model\GroupsTable;
use Application\Model\Groups;

/**
 * Class Module
 * @package Application
 */
class Module implements  ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    /**
     * @return array|mixed|\Traversable
     */

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';

    }

    /**
     * @return mixed
     */
    public static function getEnvConfig()
    {
        return include __DIR__ . '/../config/config.php';
    }

    /**
     * @return array
     */
    public function getServiceConfig(){
        return [
            'factories' => [
                RadCheckTable::class => function($container){
                    $tableGateway = $container->get(RadCheckTableGateway::class);
                    return new Model\RadCheckTable($tableGateway);
                },
                RadCheckTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RadCheck());
                    return new TableGateway('radcheck', $dbAdapter, null, $resultSetPrototype);
                },
                RadAcctTable::class => function($container){
                    $tableGateway = $container->get(RadAcctTableGateway::class);
                    return new Model\RadAcctTable($tableGateway);
                },
                RadAcctTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RadAcct());
                    return new TableGateway('radacct', $dbAdapter, null, $resultSetPrototype);
                },


                UserDevicesTable::class => function($container){
                    $tableGateway = $container->get(UserDevicesTableGateway::class);
                    return new Model\UserDevicesTable($tableGateway);
                },
                UserDevicesTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserDevices());
                    return new TableGateway('user_devices', $dbAdapter, null, $resultSetPrototype);
                },
                UserActivationRequestTable::class => function($container){
                    $tableGateway = $container->get(UserActivationRequestTableGateway::class);
                    return new Model\UserActivationRequestTable($tableGateway);
                },
                UserActivationRequestTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserActivationRequest());
                    return new TableGateway('user_activation_device', $dbAdapter, null, $resultSetPrototype);
                },

                CountriesTable::class => function($container){
                    $tableGateway = $container->get(CountriesTableGateway::class);
                    return new Model\CountriesTable($tableGateway);
                },
                CountriesTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Countries());
                    return new TableGateway('countries', $dbAdapter, null, $resultSetPrototype);
                },
                ProductUsersTable::class => function($container){
                    $tableGateway = $container->get(ProductUsersTableGateway::class);
                    return new Model\ProductUsersTable($tableGateway);
                },
                ProductUsersTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductUsers());
                    return new TableGateway('product_users', $dbAdapter, null, $resultSetPrototype);
                },
                ProductUsersProfileTable::class => function($container){
                    $tableGateway = $container->get(ProductUsersProfileTableGateway::class);
                    return new Model\ProductUsersProfileTable($tableGateway);
                },
                ProductUsersProfileTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductUsersProfile());
                    return new TableGateway('product_users_details', $dbAdapter, null, $resultSetPrototype);
                },
                GroupsTable::class => function($container){
                    $tableGateway = $container->get(GroupsTableGateway::class);
                    return new Model\GroupsTable($tableGateway);
                },
                GroupsTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Groups());
                    return new TableGateway('groups', $dbAdapter, null, $resultSetPrototype);
                },
                NasTable::class => function($container){
                    $tableGateway = $container->get(NasTableGateway::class);
                    return new Model\NasTable($tableGateway);
                },
                NasTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Nas());
                    return new TableGateway('nas', $dbAdapter, null, $resultSetPrototype);
                },
                HashTable::class => function($container){
                    $tableGateway = $container->get(HashTableGateway::class);
                    return new Model\HashTable($tableGateway);
                },
                HashTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Hash());
                    return new TableGateway('hash', $dbAdapter, null, $resultSetPrototype);
                },
                GroupLocationTable::class => function($container){
                    $tableGateway = $container->get(GroupLocationTableGateway::class);
                    return new Model\GroupLocationTable($tableGateway);
                },
                GroupLocationTableGateway::class => function($container){
                    $dbAdapter = $container->get('db1');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GroupLocation());
                    return new TableGateway('group_locations', $dbAdapter, null, $resultSetPrototype);
                },
                LocationsTable::class => function($container){
                    $tableGateway = $container->get(LocationsTableGateway::class);
                    return new Model\LocationsTable($tableGateway);
                },
                LocationsTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Locations());
                    return new TableGateway('ip2location_db1', $dbAdapter, null, $resultSetPrototype);
                },
                ProcessUsernameTable::class => function($container){
                    $tableGateway = $container->get(ProcessUsernameTableGateway::class);
                    return new Model\ProcessUsernameTable($tableGateway);
                },
                ProcessUsernameTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProcessUsername());
                    return new TableGateway('process_username', $dbAdapter, null, $resultSetPrototype);
                },
                CountryCoordsTable::class => function($container){
                    $tableGateway = $container->get(CountryCoordsTableGateway::class);
                    return new Model\CountryCoordsTable($tableGateway);
                },
                CountryCoordsTableGateway::class => function($container){
                    $dbAdapter = $container->get('db2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CountryCoords());
                    return new TableGateway('country_coords', $dbAdapter, null, $resultSetPrototype);
                },


            ],
        ];
    }

    /**
     * @return array
     */
    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class), $container->get(LocationsTable::class), $container->get(ProcessUsernameTable::class), $container->get(HashTable::class),$container->get(UserActivationRequestTable::class)

                    );
                },
                Controller\DashboardController::class => function($container) {
                    return new Controller\DashboardController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class),$container->get(UserActivationRequestTable::class),$container->get(UserDevicesTable::class),$container->get(RadAcctTable::class),$container->get(RadCheckTable::class)
                    );
                },
                Controller\ReportController::class => function($container) {
                    return new Controller\ReportController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class),$container->get(UserActivationRequestTable::class),$container->get(UserDevicesTable::class),$container->get(RadAcctTable::class),$container->get(RadCheckTable::class)
                    );
                },
                Controller\SupportController::class => function($container) {
                    return new Controller\SupportController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class),$container->get(UserActivationRequestTable::class),$container->get(UserDevicesTable::class),$container->get(RadAcctTable::class),$container->get(RadCheckTable::class)
                    );
                },
                Controller\CronController::class => function($container) {
                    return new Controller\CronController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class),$container->get(UserActivationRequestTable::class),$container->get(UserDevicesTable::class),$container->get(RadAcctTable::class),$container->get(RadCheckTable::class)
                    );
                },
                Controller\UserController::class => function($container) {
                    return new Controller\UserController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class)
                    );
                },
                Controller\TeamMemberController::class => function($container) {
                    return new Controller\TeamMemberController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class), $container->get(ProcessUsernameTable::class), $container->get(HashTable::class)
                    );
                },
                Controller\GroupController::class => function($container) {
                    return new Controller\GroupController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class), $container->get(GroupLocationTable::class)
                    );
                },
                Controller\ApiController::class => function($container) {
                    return new Controller\ApiController(
                        $container->get(CountriesTable::class), $container->get(ProductUsersTable::class), $container->get(ProductUsersProfileTable::class), $container->get(GroupsTable::class), $container->get(ProcessUsernameTable::class), $container->get(NasTable::class),$container->get(LocationsTable::class),$container->get(CountryCoordsTable::class),$container->get(UserDevicesTable::class),$container->get(RadCheckTable::class),$container->get(GroupLocationTable::class)
                    );
                },
            ],
        ];
    }
}
