<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class PasswordChangeForm
 * @package Application\Form
 */
class PasswordChangeForm extends Form
{
    /**
     * PasswordChangeForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'currentpass',
            'type' => 'Password',
            'attributes' => array(
                'placeholder' => 'Current Password',
                'id' => 'currentpass',
            ),
        ));
        $this->add(array(
            'name' => 'newpass',
            'type' => 'Password',
            'attributes' => array(
                'placeholder' => 'New Password',
                'id' => 'newpass',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'confirmpass',
            'type' => 'Password',
            'attributes' => array(
                'placeholder' => 'Confirm New Password',
                'id' => 'confirmpass', 'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Change Password',
                'id' => 'submitbutton',
                'class' => 'btn btn-warning'
            ),
        ));
    }
}

?>