<?php
namespace Application\Form;

use Zend\Form\Form;


/**
 * Class TotpForm
 * @package Users\Form
 */
class TotpForm extends Form
{

    /**
     * TotpForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'totp',
            'type' => 'text',
            'options' => array(
                'label' => 'One time Passcode',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'totp',
                'maxlength' => 6,
                'minlength' => 6,
                'placeholder' => '6-digit code'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Proceed',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary',
            ),
        ));
    }
}

?>
?>