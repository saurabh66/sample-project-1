<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class SignupForm
 * @package Application\Form
 */
class SignupForm extends Form
{
    /**
     * SignupForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'firstname',
            'type' => 'Text',
            'options' => array(
                'label' => 'First Name',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'firstname',
                'placeholder' => 'First Name',
            ),
        ));
        $this->add(array(
            'name' => 'lastname',
            'type' => 'Text',
            'options' => array(
                'label' => 'Last Name',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'lastname',
                'placeholder' => 'Last Name',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                'label' => 'Company Email',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'Company Email',
            ),
        ));

        $this->add(array(
            'name' => 'company',
            'type' => 'Text',
            'options' => array(
                'label' => 'Company',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'company',
                'placeholder' => 'Company',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'password',
                'placeholder' => 'password',
            ),
        ));
        $this->add(array(
            'name' => 'confirmpassword',
            'type' => 'Password',
            'options' => array(
                'label' => 'Confirm Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'confirmpassword',
                'placeholder' => 'Confirm Password',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Sign up',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}

?>