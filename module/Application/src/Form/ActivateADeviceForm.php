<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class ActivateADeviceForm
 * @package Application\Form
 */
class ActivateADeviceForm extends Form
{
    /**
     * ActivateADeviceForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'email',
            'type' => 'email',
            'options' => array(
                'label' => 'Email',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Send Activation Email',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}

?>