<?php

namespace Application\Form;

use Zend\Form\Form;


/**
 * Class Set2faForm
 * @package Users\Form
 */
class Set2faForm extends Form
{
    /**
     * Set2faForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'totp',
            'type' => 'text',
            'options' => array(
                'label' => 'Time-based OTP',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'set2fa',
                'placeholder' => '6-digit code'
            ),
        ));
        $this->add(array(
            'name' => 'secret',
            'type' => 'hidden',
        ));
        $this->add(array(
            'name' => 'image',
            'type' => 'hidden',
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Verify',
                'id' => 'submitset2fa',
                'class' => 'btn btn-primary',
            ),
        ));
    }
}

?>