<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class LoginForm
 * @package Application\Form
 */
class LoginForm extends Form
{
    /**
     * LoginForm constructor.
     */
    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'options' => array(
                'label' => 'User name',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'username',
                'placeholder' => 'username',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'password',
                'placeholder' => 'password',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Log in',
                'id' => 'submitbutton',
                'class' => 'btn btn-success btn-submit-send'
            ),
        ));
    }
}

?>