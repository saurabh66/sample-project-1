<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class ResetpasswordForm
 * @package Application\Form
 */
class ResetpasswordForm extends Form
{
    /**
     * ResetpasswordForm constructor.
     */
    public function __construct()
    {

        // we want to ignore the name passed
        parent::__construct('users');

        $this->add(array(
            'name' => 'email',
            'type' => 'email',
            'options' => array(
                'label' => 'Email',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Reset Password',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }

}

?>