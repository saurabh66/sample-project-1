<?php

 namespace Application\Model;

 /**
  * Class Countries
  * @package Application\Model
  */
 class Countries
 {
     /**
      * @var Int
      */
     public $id;
     /**
      * @var String
      */
     public $code;
     /**
      * @var String
      */
     public $name;

     /**
      * @param $data
      */
     public function exchangeArray($data)
     {
         $this->id    = (!empty($data['id'])) ? $data['id'] : null;
         $this->code  = (!empty($data['code'])) ? $data['code'] : null;
         $this->name  = (!empty($data['name'])) ? $data['name'] : null;
     }
 }