<?php

namespace Application\Model;

/**
 * Class GroupLocation
 * @package Application\Model
 */
class GroupLocation
{
    /**
     * @var Int
     */
    public $group_location_id;
    /**
     * @var Int
     */
    public $fk_group_id;

    /**
     * @var Double
     */
    public $country_code;
    /**
     * @var String
     */
    public $name;
    /**
     * @var String
     */
    public $code;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->group_location_id    = (!empty($data['group_location_id'])) ? $data['group_location_id'] : null;
        $this->fk_group_id  = (!empty($data['fk_group_id'])) ? $data['fk_group_id'] : null;
        $this->name  = (!empty($data['name'])) ? $data['name'] : null;
        $this->fk_country_id  = (!empty($data['fk_country_id'])) ? $data['fk_country_id'] : null;
        $this->code  = (!empty($data['code'])) ? $data['code'] : null;
    }
}