<?php

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select as Select;

/**
 * Class CountriesTable
 * @package Application\Model
 */
class CountriesTable
{
    /**
     * @var TableGatewayInterface
     */
    protected $tableGateway;

    /**
     * CountriesTable constructor.
     * @param TableGatewayInterface $tableGateway
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    /**
     * @return mixed
     *
     */
    public function fetchAll()
    {
        $supported_countries = array("AU","AT","CA","CH","DE","ES","FI","FR","IE","IL","IN","IT","NL","RU","SE","SG","UK","US");

        $select = new Select();
        $select->from('countries')->where(array('code' => $supported_countries))->order('name ASC');
        return $this->tableGateway->selectWith($select); 
    }

    /**
     * @param $iso
     * @return mixed
     */
    public function getCountryNameByIso($iso)
    {
        return $resultSet = $this->tableGateway->select(array('code' => $iso))->current();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCountryNameById($id){

        return $resultSet = $this->tableGateway->select(array('id' => $id))->current();
    }
}
?>
