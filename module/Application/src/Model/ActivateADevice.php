<?php

namespace Application\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


/**
 * Class ActivateADevice
 * @package Application\Model
 */
class ActivateADevice implements InputFilterAwareInterface
{
    /**
     * @var Email
     */
    public $email;
    /**
     * @var Input Filter
     */
    protected $inputFilter;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->email = (isset($data['email'])) ? $data['email'] : null;
    }

    // Add content to these methods:

    /**
     * @param InputFilterInterface $inputFilter
     * @return void|InputFilterAwareInterface
     * @throws \Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * @return Input|InputFilter|InputFilterInterface
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $email_validator = new \Zend\Validator\EmailAddress();
            $email_validator->setOptions(array('deep' => true));

            $inputFilter->add(array(
                'name' => 'email',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                //  'validators' => array(),

                'validators' => array(
                    array(
                        'name' =>'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'You must enter an email address.'
                            ),
                        ),
                    ),
                    $email_validator
                ),

            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
?>
