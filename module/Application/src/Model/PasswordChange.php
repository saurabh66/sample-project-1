<?php

namespace Application\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Application\Model\PasswordStrength;

/**
 * Class PasswordChange
 * @package Application\Model
 */
class PasswordChange implements InputFilterAwareInterface
{
    public $currentpass;
    public $newpass;
    public $confirmpass;
    protected $inputFilter;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->newpass = (isset($data['newpass'])) ? $data['newpass'] : null;
        $this->confirmpass = (isset($data['confirmpass'])) ? $data['confirmpass'] : null;
    }

    // Add content to these methods:

    /**
     * @param InputFilterInterface $inputFilter
     * @return void|InputFilterAwareInterface
     * @throws \Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * @return InputFilter|InputFilterInterface
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $password_validator = new PasswordStrength();

            $inputFilter->add(array(
                'name' => 'newpass',
                'required' => true,
                'validators' => array($password_validator),
            ));

            $inputFilter->add(array(
                'name' => 'confirmpass',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => 'newpass',
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
?>