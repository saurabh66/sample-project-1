<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select as Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Zend\Crypt\Password\Bcrypt;


/**
 * Class NasTable
 * @package Application\Model
 */
class NasTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * NasTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select([ 'attribute' => 'Cleartext-Password', 'op'=> ':=' ]);
        return $resultSet;
    }

    /**
     * @param $id
     * @return array|\ArrayObject|bool|null
     */
    public function getNasById($id)
    {
        $resultSet = $this->tableGateway->select(array('id'=>$id));
        if($resultSet->count()>0)
        {
            return $resultSet->current();
        } else {
            return false;
        }
    }


    /**
     * @todo Get NAS by nasname
     */
    /**
     * @param $nasname
     * @return int
     */
    public function deleteNasByName($nasname)
    {
        return $resultSet = $this->tableGateway->delete(array('nasname'=>$nasname));
    }

    /**
     * @todo Add Nas table
     */
    /**
     * @param $data
     * @return int
     */
    public function addNas($data)
    {
        return $this->tableGateway->insert(
            array(
                'nasname' => $data['nasname'],
                'shortname' => $data['shortname'],
                'type' => 'other',
                'secret' => $data['secret'],
                'server' => NULL,
                'community' => NULL,
                'community' => NULL,
                'description' => $data['description'],
                'online' => 1,
                'country' => $data['country'],
                'added' => new \Zend\Db\Sql\Expression("NOW()"),
            )
        );
    }


    /**
     * @todo Get closest NAS to a country
     */
    /**
     * @param $countryCoords
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getClosestNases($countryCoords)
    {
        $sql = "SELECT DISTINCT nasname, A.CC as ISO,B.country FROM country_coords A, nas B WHERE (A.CC = B.country) AND (B.online = 1) ORDER BY ABS(ABS(A.`Latitude`-({$countryCoords->Latitude})) + ABS(A.`Longitude`-({$countryCoords->Longitude}))) ASC LIMIT 5;";

        return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
    }

    /**
     * @param $id
     * @return array|\ArrayObject|bool|null
     */
    public function getNasByIp($id)
    {
        $resultSet = $this->tableGateway->select(array('nasname'=>$id));
        if($resultSet->count()>0)
        {
            return $resultSet->current();
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getNasesByCode($data)
    {
        $resultSet = $this->tableGateway->select(function(Select $select) use ($data){
            $select->where(array('country' => $data));
            $rand = new \Zend\Db\Sql\Expression('RAND()');
            $select->order($rand);
            $select->limit(5);
        });
        return $resultSet;
    }

    /**
     * @param $fixedIp
     * @param null $backupId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getNasData($fixedIp,$backupId=null){
        $nasSql = "SELECT nasname,country as country_name from nas where nasname = '$fixedIp' and online = 1;";

        return $this->tableGateway->getAdapter()->driver->getConnection()->execute($nasSql);
    }


    /**
     * @param $country
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getClosestNasesByCountryCode($country){

        $in = implode("','",$country);

        foreach($country as $c){
          
            $nasSql = "SELECT nasname,country as country_name from nas where country in ('" .$in. "') and online = 1;";
            return $this->tableGateway->getAdapter()->driver->getConnection()->execute($nasSql);
        }
    }


}
?>
