<?php

namespace Application\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Class Set2fa
 * @package Users\Model
 */
class Set2fa implements InputFilterAwareInterface
{
    public $totp;
    public $secret;
    public $image;
    protected $inputFilter;


    /**
     * Array Exchanger method
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->totp = (isset($data['totp'])) ? $data['totp'] : null;
    }

    /**
     * Set input filter
     * @param InputFilterInterface $inputFilter
     * @return void|InputFilterAwareInterface
     * @throws \Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * get input filter
     * @return InputFilter|InputFilterInterface
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'totp',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 6,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'totp',
                'required' => true
            ));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
?>