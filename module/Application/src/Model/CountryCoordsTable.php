<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select as Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Zend\Crypt\Password\Bcrypt;
use Users\Model\RadCheck;

/**
 * Class CountryCoordsTable
 * @package Application\Model
 */
class CountryCoordsTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * CountryCoordsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select([ 'attribute' => 'Cleartext-Password', 'op'=> ':=' ]);
        return $resultSet;
    }

    /**
     * @param $iso
     * @return array|\ArrayObject|null
     */
    public function getCountryByIso($iso)
    {
        return $resultSet = $this->tableGateway->select(array('CC'=>$iso))->current();
    }
}
?>
