<?php
namespace Application\Model;

/**
 * Class CountryCoords
 * @package Application\Model
 */
class CountryCoords
{
    /**
     * @var String
     */
    public $CC;
    /**
     * @var Double
     */
    public $Latitude;
    /**
     * @var Double
     */
    public $Longitude;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->CC   = (!empty($data['CC'])) ? $data['CC'] : null;
        $this->Latitude   = (!empty($data['Latitude'])) ? $data['Latitude'] : null;
        $this->Longitude   = (!empty($data['Longitude'])) ? $data['Longitude'] : null;
    }
}
