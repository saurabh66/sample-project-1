<?php

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select as Select;
use Zend\Db\Sql\Where;


/**
 * Class GroupLocationTable
 * @package Application\Model
 */
class GroupLocationTable
{
    /**
     * @var TableGatewayInterface
     */
    protected $tableGateway;

    /**
     * GroupLocationTable constructor.
     * @param TableGatewayInterface $tableGateway
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $group_id
     * @return bool
     */
    public function fetchAll($group_id)
    {
        $where = new Where();
        $where->equalTo('fk_group_id', $group_id);
        $resultSet = $this->tableGateway->select(function(Select $select) use ($where){
            $select->join(array('countries' => 'countries'), 'countries.id = group_locations.fk_country_id');
            $select->where($where);
        });
        if($resultSet->count() > 0) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * @param $countryId
     * @param $groupId
     */
    public function insertCountries($countryId,$groupId)
    {
        $this->tableGateway->insert(["fk_country_id"=>$countryId,"fk_group_id"=>$groupId]);
    }

    /**
     * @param $groupId
     */
    public function deleteCountries($groupId)
    {
        $this->tableGateway->delete(["fk_group_id"=>$groupId]);
    }

    /**
     * @param $groupId
     * @return mixed
     */
    public function getNasDataGeoCase($groupId){

        $sql = "SELECT * from group_locations where fk_group_id =". $groupId;
        return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
    }

}
?>
