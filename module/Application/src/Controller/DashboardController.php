<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Classes\WHMCS\CallWHMCS;
use Application\Module;
use Application\Model\UserActivationRequestTable;
use Application\Model\CountriesTable;
use Application\Model\ProductUsersTable;
use Application\Model\GroupsTable;
use Application\Model\UserDevicesTable;
use Application\Model\ProductUsersProfileTable;
use Application\Model\RadAcctTable;
use Application\Model\RadCheckTable;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

/**
 * Class DashboardController
 * @package Application\Controller
 */
class DashboardController extends AbstractActionController
{
    /**
     * @var CountriesTable|null
     */
    private $countriesTable;
    /**
     * @var ProductUsersTable|null
     */
    private $productUserTable;
    /**
     * @var ProductUsersProfileTable|null
     */
    private $productUsersProfileTable;
    /**
     * @var GroupsTable|null
     */
    private $groupsTable;
    /**
     * @var UserActivationRequestTable|null
     */
    private $UserActivationRequestTable;
    /**
     * @var UserDevicesTable|null
     */
    private $UserDevicesTable;
    /**
     * @var RadAcctTable|null
     */
    private $RadAcctTable;
    /**
     * @var RadCheckTable|null
     */
    private $RadCheckTable;

    /**
     * DashboardController constructor.
     * @param CountriesTable|null $countriesTable
     * @param ProductUsersTable|null $productUserTable
     * @param ProductUsersProfileTable|null $productUsersProfileTable
     * @param GroupsTable|null $groupsTable
     * @param UserActivationRequestTable|null $UserActivationRequestTable
     * @param UserDevicesTable|null $UserDevicesTable
     * @param RadAcctTable|null $RadAcctTable
     * @param RadCheckTable|null $RadCheckTable
     */
    public function __construct(CountriesTable $countriesTable = null, ProductUsersTable $productUserTable = null, ProductUsersProfileTable $productUsersProfileTable = null, GroupsTable $groupsTable = null, UserActivationRequestTable $UserActivationRequestTable = null, UserDevicesTable $UserDevicesTable = null, RadAcctTable $RadAcctTable = null, RadCheckTable $RadCheckTable = null)
    {
        $this->RadCheckTable = $RadCheckTable;
        $this->RadAcctTable = $RadAcctTable;
        $this->UserDevicesTable = $UserDevicesTable;
        $this->UserActivationRequestTable = $UserActivationRequestTable;
        $this->countriesTable = $countriesTable;
        $this->productUserTable = $productUserTable;
        $this->productUsersProfileTable = $productUsersProfileTable;
        $this->groupsTable = $groupsTable;
    }

    /**
     * @return \Zend\Http\Response
     * Checking Sesssion log
     */
    public function check_login_after()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        if (!isset($_SESSION['userid'])) {
            return $this->redirect()->toRoute('login');
        } else {
            if ($this->productUserTable->getLoginCheck($_SESSION['userid'])) {

            } else {
                unset($_SESSION['userid']);
                unset($_SESSION['username']);
                session_destroy();
                return $this->redirect()->toRoute('login');
            }
        }
    }

    /**
     * @return \Zend\Http\Response
     *
     * Logout Action
     * unset session
     */
    public function logoutAction()
    {
        $this->check_login_after();
        $sm = new \Zend\Session\SessionManager;
        if (isset($_SESSION['admin_id'])) {
            $sm->destroy();
            header('Location: https://partners.privatise.com');
            die();
        }
        $sm->destroy();
        return $this->redirect()->toRoute('login');

    }

    /**
     * @return \Zend\Http\Response
     * Checking Sesssion log using AJAX
     */
    public function checkLoginAjax()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        if (!isset($_SESSION['userid'])) {
            echo json_encode(array('status' => 'Unauthrosied'));
            die;
        }
    }

    /**
     * Activation link send
     */
    function activationAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $this->checkLoginAjax();
        $data = explode("&", $this->getRequest()->getPost()['data']);
        unset($data[count($data) - 1]);
        foreach ($data as $un):
            $user = $this->productUserTable->getByUsername($un);
            if ($user) {
                $config = Module::getEnvConfig();
                $selfInvite = $this->productUserTable->getActivationExpiry($_SESSION['userid']);
                $tokenTime = strtotime($user->email_activation_time) + $selfInvite;
                if ($tokenTime < time()) {
                    $user->email_activation_hash = md5($user->clientid . $user->productid . time());
                }
                $user->email_activation_time = new \Zend\Db\Sql\Expression("NOW()");
                $user->invitation_type = 2;
                if ($un != $_SESSION['userid']):
                    $user->invited_by = $_SESSION['userid'];
                endif;
                $this->productUserTable->update($user);
                $inviter = $this->productUsersProfileTable->getSingleRecord($_SESSION['userid']);
                if ($un == $_SESSION['userid']):
                    /** Send email with Download installer link */
                    $message_text = file_get_contents(__DIR__ . '/../Form/SendEmail.email');
                    $message_text = str_replace(array('$firstname', '$lastname', '$link', '$servername'), array($inviter->firstname, $inviter->lastname, $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user->email_activation_hash), $_SERVER['SERVER_NAME']), $message_text);
                else:

                    $message_text = file_get_contents(__DIR__ . '/../Form/BulkInviteTeam.email');
                    $message_text = str_replace(array(
                        '$link',
                        '$servername',
                        '$invitername',
                        '$username',
                        '$password'), array(
                        $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user->email_activation_hash),
                        $_SERVER['SERVER_NAME'],
                        $inviter->firstname . " " . $inviter->lastname,
                        $_SESSION['email'],
                        ''
                    ), $message_text);


                endif;
                $config = Module::getEnvConfig();
                $html = new MimePart($message_text);
                $html->type = "text/html";
                $body = new MimeMessage();
                $body->setParts(array($html));
                $mail = new Mail\Message();
                $mail->setBody($body)
                    ->setFrom($config['fromEmail'], $inviter->firstname . ' ' . $inviter->lastname . ' at' . ' Privatise')
                    ->addTo($user->email)
                    ->setSubject($config['EmailSubject']['bultInvite']);
                $transport = new Mail\Transport\Smtp();
                $options = new Mail\Transport\SmtpOptions(array(
                    'host' => $config['HOSTNAME'],
                    'port' => $config['PORT'],
                    'connection_class' => 'login',
                    'connection_config' => array(
                        'username' => $config['USERNAME'],
                        'password' => $config['PASSWORD'],
                        'ssl' => $config['SSL'],
                    ),
                ));
                $transport->setOptions($options);
                $transport->send($mail);
                $data['status'] = 200;
                $data['message'] = 'Please check your email for the Activation link.';
            }
        endforeach;
        die;
    }

    /**
     * Actionavtion Link Request
     */
    function activationrequestAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $this->checkLoginAjax();
        $data = explode("&", $this->getRequest()->getPost()['data']);
        unset($data[count($data) - 1]);
        foreach ($data as $un):
            $user = $this->productUserTable->getByUsername($un);
            if ($user) {
                $config = Module::getEnvConfig();
                $selfInvite = $this->productUserTable->getActivationExpiry($_SESSION['userid']);
                /** Update email activation token */
                $tokenTime = strtotime($user->email_activation_time) + $selfInvite;
                if ($tokenTime < time()) {
                    $user->email_activation_hash = md5($user->clientid . $user->productid . time());
                }
                $user->email_activation_time = new \Zend\Db\Sql\Expression("NOW()");
                $user->invitation_type = 2;
                if ($un != $_SESSION['userid']):
                    $user->invited_by = $_SESSION['userid'];
                endif;
                $this->productUserTable->update($user);
                $inviter = $this->productUsersProfileTable->getSingleRecord($_SESSION['userid']);
                $this->UserActivationRequestTable->UpdateActivationRequest($un);
                if ($un == $_SESSION['userid']):
                    /** Send email with Download installer link */
                    $message_text = file_get_contents(__DIR__ . '/../Form/SendEmail.email');
                    $message_text = str_replace(array('$firstname', '$lastname', '$link', '$servername'), array($inviter->firstname, $inviter->lastname, $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user->email_activation_hash), $_SERVER['SERVER_NAME']), $message_text);
                else:
                    $message_text = file_get_contents(__DIR__ . '/../Form/BulkInviteTeam.email');
                    $message_text = str_replace(array(
                        '$link',
                        '$servername',
                        '$invitername',
                        '$username',
                        '$password'), array(
                        $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user->email_activation_hash),
                        $_SERVER['SERVER_NAME'],
                        $inviter->firstname . " " . $inviter->lastname,
                        $_SESSION['email'],
                        ''
                    ), $message_text);
                endif;
                $config = Module::getEnvConfig();
                $html = new MimePart($message_text);
                $html->type = "text/html";
                $body = new MimeMessage();
                $body->setParts(array($html));
                $mail = new Mail\Message();
                $mail->setBody($body)
                    ->setFrom($config['fromEmail'], $config['fromName'])
                    ->addTo($user->email)
                    ->setSubject($config['EmailSubject']['bultInvite']);
                $transport = new Mail\Transport\Smtp();
                $options = new Mail\Transport\SmtpOptions(array(
                    'host' => $config['HOSTNAME'],
                    'port' => $config['PORT'],
                    'connection_class' => 'login',
                    'connection_config' => array(
                        'username' => $config['USERNAME'],
                        'password' => $config['PASSWORD'],
                        'ssl' => $config['SSL'],
                    ),
                ));
                $transport->setOptions($options);
                $transport->send($mail);

                $data['status'] = 200;
                $data['message'] = 'Please check your email for the Activation link.';
            }
        endforeach;
        $activation = $this->UserActivationRequestTable->fetchAll($_SESSION['userid']);
        echo $activation->count();
        die;
    }

    /**
     * @return array
     * Home Page || Dashboard Page
     */
    function indexAction()
    {
        $myTeamMembers = [];
        $this->check_login_after();
        $config = Module::getEnvConfig();
        $user['recent'] = $this->UserDevicesTable->fetch_count_recent($_SESSION['userid'], $config['dashboard']['users']['recent']);
        $user['active_last'] = $this->UserDevicesTable->fetch_count_recent($_SESSION['userid'], $config['dashboard']['users']['active_last_start'], $config['dashboard']['users']['active_last_end']);
        $user['inactive'] = $this->UserDevicesTable->fetch_count_last($_SESSION['userid'], $config['dashboard']['users']['inactive']);
        $device['recent'] = $this->UserDevicesTable->fetch_device_count_recent($_SESSION['userid'], $config['dashboard']['device']['recent']);
        $device['active_last'] = $this->UserDevicesTable->fetch_device_count_recent($_SESSION['userid'], $config['dashboard']['device']['active_last_start'], $config['dashboard']['device']['active_last_end']);
        $device['inactive'] = $this->UserDevicesTable->fetch_device_count_last($_SESSION['userid'], $config['dashboard']['device']['inactive']);
        $data = '';
        $countries = $this->countriesTable->fetchAll();
        $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
        $checkIpEnabledGroup = $this->groupsTable->checkIfEnabled($currentUserData->id);
        $userdata = $currentUserData;
        $cu = [];

        $avghrs = $this->RadAcctTable->getRadData($userdata->username, 'Time Period');
        $cu['totalUsage'] = $avghrs['usage'];
        $cu['loggedIn'] = $this->UserDevicesTable->UserDevicesIconByUsername($userdata->username);


        $WHMCS = new CallWHMCS();


        $clientWHMCS = json_decode($WHMCS->getResponse("getclients", ["search" => $currentUserData->email], "json"));

        $client = $clientWHMCS->clients->client[0];


        $clientOtherDetailsWHMCS = json_decode($WHMCS->getResponse("GetClientsDetails", ["stats" => true, "clientid" => $currentUserData->clientid], "json"));

        $clientOtherDetails = $clientOtherDetailsWHMCS->client;

        $this->layout()->setVariable('countries', $countries);
        $this->layout()->setVariable('countryCode', $clientOtherDetails->countrycode);
        $this->layout()->setVariable('currentUserData', $currentUserData);
        $this->layout()->setVariable('title', "Dashboard");
        $this->layout()->getOptions(["title" => "about"]);

        $this->layout()->setVariable('company', $client->companyname);
        $team = ($this->productUserTable->getTeamMembersHome($currentUserData->id));
        $getActivationRequestList = $this->UserActivationRequestTable->fetchAll($_SESSION['userid']);

        if ($team) {
            $myTeamMembers = [];

            foreach ($team as $eachMember) {

                if ($eachMember->status != 2) {

                    $tmp = (array)$eachMember;
                    $avghrs = $this->RadAcctTable->getRadData($eachMember->username, 'Time Period');
                    $tmp['totalUsage'] = $avghrs['usage'];
                    $tmp['loggedIn'] = $this->UserDevicesTable->UserDevicesIconByUsername($eachMember->username);
                    $myTeamMembers[] = $tmp;
                    unset($tmp);
                }
            }

            $data = ($this->productUserTable->getTeamMembersHome($currentUserData->id));
        }
        return ["data" => $data, "countries" => $countries, "ActivationRequestList" => $getActivationRequestList, "myTeam" => $myTeamMembers, "userdata" => $userdata, "cu" => $cu, "users" => $user, "devices" => $device, "checkIpEnabledGroup" => $checkIpEnabledGroup];
    }

    /**
     * Advance Tab
     * @return array
     */
    function advanceAction()
    {
        $this->check_login_after();
        $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
        $this->layout()->setVariable('currentUserData', $currentUserData);
        $this->layout()->setVariable('title', "Advance");
        return ["users" => $currentUserData];
    }

    /**
     * Advance Submit Tab
     */
    function advancesubmitAction()
    {
        $this->check_login_after();
        $tag = array();
        $error = array();
        foreach (array('log_retaintion', 'expirationDays', 'expirationHrs', 'expirationMins') as $item) {
            if (strlen(trim($this->getRequest()->getPost()[$item])) == 0) {
                $tag[] = $item;
                $error[] = ucwords($item) . ' field is required';
            }
        }
        if (count($error) == 0) {
            $log_retaintion = $this->getRequest()->getPost()['log_retaintion'];
            $id = $_SESSION['userid'];
            $days = $this->getRequest()->getPost()['expirationDays'];
            $hours = $this->getRequest()->getPost()['expirationHrs'];
            $minutes = $this->getRequest()->getPost()['expirationMins'];
            $activation_expriy = $minutes + ($hours * 60) + ($days * 24 * 60);
            $this->productUserTable->updateAdvance($id, $activation_expriy, $log_retaintion);
            $data = ["status" => "success"];
        } else {
            $data = ["status" => "error", "tags" => $tag, "error" => $error];
        }
        echo json_encode($data);
        die;
    }

    /**
     *  Feedback Form mail send and validation
     */
    function contactsubmitAction()
    {
        $this->checkLoginAjax();
        $tag = array();
        $error = array();
        foreach (array('name', 'message', 'email') as $item) {
            if (strlen(trim($this->getRequest()->getPost()[$item])) == 0) {

                $tag[] = $item;
                $error[] = ucwords($item) . ' field is required';

            }
        }
        if ($this->getRequest()->getPost()['email'] != "") {
            $value = $this->getRequest()->getPost()['email'];
            $correct = preg_match("/^[0-9a-zA-Z\.\-\_\+]*?\@[0-9a-zA-Z][0-9a-zA-Z\.\-]*?\.[a-zA-Z]{2,}$/", $value);

            if (!$correct) {
                $tag[] = 'email';
                $error[] = 'Incorrect email format';
            }
            unset($value);
        }
        if (count($error) == 0) {   /*  Send mail to admin */
            $message_text = file_get_contents(__DIR__ . '/../Form/AdminContact.email');
            $message_text = str_replace(
                array('$name', '$email', '$message', '$phone'),
                array($this->getRequest()->getPost()['name'], $this->getRequest()->getPost()['email'], $this->getRequest()->getPost()['message'], $this->getRequest()->getPost()['phone']), $message_text);
            $config = Module::getEnvConfig();
            $html = new MimePart($message_text);
            $html->type = "text/html";
            $body = new MimeMessage();
            $body->setParts(array($html));
            $mail = new Mail\Message();
            $mail->setBody($body)
                ->setFrom($config['fromEmail'], $config['fromName'])
                ->addTo($config['contactEmail'])
                ->setSubject($config['EmailSubject']['ContactUS']);
            $transport = new Mail\Transport\Smtp();
            $options = new Mail\Transport\SmtpOptions(array(
                'host' => $config['HOSTNAME'],
                'port' => $config['PORT'],
                'connection_class' => 'login',
                'connection_config' => array(
                    'username' => $config['USERNAME'],
                    'password' => $config['PASSWORD'],
                    'ssl' => $config['SSL'],
                ),
            ));
            $transport->setOptions($options);
            $transport->send($mail);
            /*  Send mail to Client */
            $message_text = file_get_contents(__DIR__ . '/../Form/ClientContact.email');
            $message_text = str_replace(
                array('$name'),
                array($this->getRequest()->getPost()['name']), $message_text);
            $config = Module::getEnvConfig();
            $html = new MimePart($message_text);
            $html->type = "text/html";
            $body = new MimeMessage();
            $body->setParts(array($html));
            $mail = new Mail\Message();
            $mail->setBody($body)
                ->setFrom($config['fromEmail'], $config['fromName'])
                ->addTo($this->getRequest()->getPost()['email'])
                ->setSubject($config['EmailSubject']['ContactUsUser']);

            $transport = new Mail\Transport\Smtp();
            $options = new Mail\Transport\SmtpOptions(array(
                'host' => $config['HOSTNAME'],
                'port' => $config['PORT'],
                'connection_class' => 'login',
                'connection_config' => array(
                    'username' => $config['USERNAME'],
                    'password' => $config['PASSWORD'],
                    'ssl' => $config['SSL'],
                ),
            ));
            $transport->setOptions($options);
            $transport->send($mail);
            echo json_encode(array('status' => 'Success'));
        } else {
            echo json_encode(array('status' => 'Error', 'tag' => $tag, 'error' => $error));
        }
        exit;
    }
}
