<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Classes\WHMCS\CallWHMCS;
use Application\Module;
use Application\Model\RadCheckTable;
use Application\Model\CountriesTable;
use Application\Model\NasTable;
use Application\Model\ProductUsersTable;
use Application\Model\GroupsTable;
use Application\Model\LocationsTable;
use Application\Model\UserDevicesTable;
use Application\Model\CountryCoordsTable;
use Application\Model\ProcessUsernameTable;
use Application\Model\ProductUsersProfileTable;
use Application\Model\GroupLocationTable;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Crypt\BlockCipher;
use Zend\Db\Sql\Expression;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;

/**
 * Class ApiController
 * API controller for iOS, droid, MacOS and Windoze client
 * @package Application\Controller
 */
class ApiController extends AbstractActionController
{
    /**
     * Countries Table Adapter
     * @var CountriesTable
     */
    private $countriesTable;
    /**
     * Products table adapter
     * @var ProductUsersTable
     */
    private $productUserTable;
    /**
     * Product Users profile table adapter
     * @var ProductUsersProfileTable
     */
    private $productUsersProfileTable;
    /**
     * Groups table adapter
     * @var GroupsTable
     */
    private $groupsTable;
    /**
     * Products Username table adapter
     * @var ProcessUsernameTable
     */
    private $ProcessUsername;
    /**
     * Nas table adapter from db->freeradiusnew
     * @var NasTable
     */
    private $Nas;
    /**
     * Locations table adapte
     * @var LocationsTable
     */
    private $location;
    /**
     * User Devices log table
     * @var UserDevicesTable
     */
    private $UserDevices;
    /**
     * Country Coords Table adapter
     * @var CountryCoordsTable
     */
    private $coords;
    /**
     * RadCheck Table adapter
     * @var RadCheckTable
     */
    private $RadCheckTable;
    /**
     * Group Location Table Adapter
     * @var GroupLocationTable
     */
    private $GroupLocationTable;
    /**
     * Radius server API authentication key
     * @var string
     */
    public $apiToken = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

    /**
     * ApiController constructor.
     * @param CountriesTable|null $countriesTable
     * @param ProductUsersTable|null $productUserTable
     * @param ProductUsersProfileTable|null $productUsersProfileTable
     * @param GroupsTable|null $groupsTable
     * @param ProcessUsernameTable|null $ProcessUsername
     * @param NasTable|null $Nas
     * @param LocationsTable|null $location
     * @param CountryCoordsTable|null $coords
     * @param UserDevicesTable|null $userdevice
     * @param RadCheckTable|null $RadCheckTable
     * @param GroupLocationTable|null $GroupLocationTable
     */
    public function __construct(CountriesTable $countriesTable = null, ProductUsersTable $productUserTable = null, ProductUsersProfileTable $productUsersProfileTable = null, GroupsTable $groupsTable = null, ProcessUsernameTable $ProcessUsername = null, NasTable $Nas = null, LocationsTable $location = null, CountryCoordsTable $coords = null, UserDevicesTable $userdevice = null, RadCheckTable $RadCheckTable = null, GroupLocationTable $GroupLocationTable = null)
    {
        $this->RadCheckTable = $RadCheckTable;
        $this->GroupLocationTable = $GroupLocationTable;
        $this->coords = $coords;
        $this->countriesTable = $countriesTable;
        $this->UserDevices = $userdevice;
        $this->productUserTable = $productUserTable;
        $this->productUsersProfileTable = $productUsersProfileTable;
        $this->location = $location;
        $this->groupsTable = $groupsTable;
        $this->Nas = $Nas;
        $this->ProcessUsername = $ProcessUsername;


    }

    /**
     * Add ticket
     * @package WHMCS APIs
     */
    public function addTicketViewAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * @todo Update Client OS in Product Username Table
             */

            $key = $request->getPost('key');
            $radUser = $request->getPost('username');
            $subject = $request->getPost('subject');
            $message = $request->getPost('message');
            $logFile = $request->getPost('logfile');

            if ($key && $radUser) {

                /** start http request */
                $WHMCS = new CallWHMCS();
                $user = $this->productUserTable->getByUsernames($radUser);

                $postfields = array();
                $postfields["subject"] = $subject;
                $postfields["message"] = $message;
                $postfields["name"] = $user->firstname . ' ' . $user->lastname;
                $postfields["email"] = $user->email;
                $postfields["deptid"] = 1;
                $postfields["responsetype"] = "json";

                $json = $WHMCS->getResponse("OpenTicket", $postfields, "json");

                if (!$json) {
                    return false;
                } else {
                    $data = json_decode($json, true);

                    if ($logFile) {
                        $logFile = base64_decode($logFile);

                        $ticketno = $data['id'];
                        $notepostfields = array();

                        $notepostfields["message"] = $logFile;
                        $notepostfields["ticketid"] = $data['id'];

                        $json = $WHMCS->getResponse("AddTicketNote", $notepostfields, "json");;
                        $data = json_decode($json, 1);

                        if ((!$json || $data['result'] == 'error')) {

                            $error = 'Error while attaching log file, Please try again';
                        } else {

                        }

                    }

                }

            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }


        }
        $param = ['key' => $key, 'username' => $radUser];

        return $this->redirect()->toRoute('get-tickets-view', [], array('query' => $param));

        echo json_encode($data);
        die;

    }


    /**
     * Post reply in a ticket Action
     * @return redirect
     */
    public function postTicketReplyAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);

        $request = $this->getRequest();

        /**
         * API Requests params
         */
        $key = $request->getQuery('key');
        $radUser = $request->getQuery('username');
        $ticketNum = $request->getQuery('tid');


        if (!empty($key)) {

            if ($radUser && $ticketNum) {
                $reply = $request->getPost('reply');
                if (!$reply) {
                    $replyPostStatus = 'fail';
                } else {


                    /** start http request */
                    $WHMCS = new CallWHMCS();
                    $user = $this->productUserTable->getByUsernames($radUser);
                    $data = [];

                    $postfields = array();
                    $postfields["action"] = "AddTicketReply";
                    $postfields["ticketid"] = $ticketNum;
                    $postfields["message"] = $reply;
                    $postfields["name"] = $request->getQuery('usrOrigName');
                    $postfields["email"] = $request->getQuery('usrEmail');

                    $json = $WHMCS->getResponse("AddTicketReply", $postfields, "json");

                    if (!$json) {
                        $replyPostStatus = 'Error while posting reply, Please try again';
                    } else {
                        $data = json_decode($json);
                        $replyPostStatus = 'success';
                    }
                }

            } else {
                // Show error
                $replyPostStatus = 'Unauthorized access!';
            }

        } else {
            $replyPostStatus = 'Access Denied !';
        }

        $param = ['tid' => $ticketNum, 'key' => $key, 'username' => $radUser];

        $session = new Container('base');
        $session->offsetSet('replyPostStatus', $replyPostStatus);

        return $this->redirect()->toRoute('ticket-details', [], array('query' => $param));
    }


    /**
     * Get Ticket Details Action
     * @return array
     */
    public function ticketDetailsAction()
    {

        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        //$error='';
        $request = $this->getRequest();

        /**
         * API Requests params
         */
        $key = $request->getQuery('key');
        $radUser = $request->getQuery('username');
        $ticketNum = $request->getQuery('tid');
        $error = '';


        if ($radUser && $ticketNum) {

            /** start http request */
            $WHMCS = new CallWHMCS();

            $user = (object)$this->productUserTable->getByUsernames($radUser);
            $data = [];


            $postfields = array();

            $postfields["ticketid"] = $ticketNum;

            $json = $WHMCS->getResponse("GetTicket", $postfields, "json");

            if (!$json) {

                $error = 'Error while getting tickets details, Please try again';
            } else {

                $data = json_decode($json);
            }

        } else {
            // Show error
            $error = 'Please specify a username and Ticket ID';
        }


        $session = new Container('base');
        if ($session->offsetExists('replyPostStatus')) {
            $replyPostStatus = $session->offsetGet('replyPostStatus');
            $session->offsetUnset('replyPostStatus');
        } else
            $replyPostStatus = '';
        $this->layout()->setTemplate('layout/newLayout');
        return ['details' => $data, 'error' => $error, 'key' => $key, 'username' => $radUser, 'replyPostStatus' => $replyPostStatus];
    }

    /**
     * Get Tickets List
     * @package WHMCS APIs
     */
    public function getTicketDetailAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getPost('key');
            $username = $request->getPost('username');
            $ticketNum = $request->getPost('ticketnum');

            if ($key && $username) {
                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    /** start http request */
                    $WHMCS = new CallWHMCS();
                    $postfields = array();
                    $postfields["ticketid"] = $ticketNum;
                    $json = $WHMCS->getResponse("GetTicket", $postfields, "json");

                    if (!$json) {
                        return false;
                    } else {
                        $data = json_decode($json, true);
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }


        }

        return new JsonModel($data);
    }


    /**
     * Get Tickets Action
     * @return array
     */
    public function getTicketsAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);

        $request = $this->getRequest();

        /**
         * API Requests params
         */
        $key = 1;
        $radUser = trim($request->getQuery('username'));
        $teamUser = trim($request->getQuery('teamuser'));

        if ($radUser) {

            /** start http request */
            $WHMCS = new CallWHMCS();

            $user = (object)$this->productUserTable->getByUsernames($radUser);

            if ($user) {

                $teamMembers = false;

                if ($user->role == 'Full Admin') {
                    $teamMembers = $this->productUserTable->getTeamMembers($user->id);
                }

                $requestEmail = $user->email;

                if (strlen($teamUser) > 0) {
                    $teamMemberUser = $this->productUserTable->getByUsernames($teamUser);

                    if ($teamMemberUser) {
                        $requestEmail = $teamMemberUser->email;
                    }

                }

                $data = [];
                $postfields = array();
                $postfields["email"] = $requestEmail;
                $json = $WHMCS->getResponse("GetTickets", $postfields, "json");

                if (!$json) {

                    $error = 'Error in getting clients tickets, Please try again';
                } else {

                    $json = json_decode($json);
                    $data = $json->tickets->ticket;
                    $totalresults = $json->totalresults;
                }
            } else {
                $error = 'Please specify a valid username';
            }

        } else {
            // Show error
            $error = 'Please specify a username';
        }

        $this->layout()->setTemplate('layout/newLayout');

        return ['tickets' => $data, 'error' => $error, 'key' => $key, 'radUser' => $radUser, 'teamUser' => $teamUser, 'teamMembers' => $teamMembers, 'user' => $user];
    }

    public function indexAction()
    {
        $data = ['version' => '1.0'];
        return new JsonModel($data);
    }

    /**
     * @todo Send Invitation Link
     */
    public function downloadInstallerAction()
    {
        $request = $this->getRequest();
        $config = Module::getEnvConfig();

        if ($request->isGet()) {

            $hash = $request->getQuery('hash');

            if ($hash) {
                /**
                 * @todo Update Client OS in Product Username Table
                 */
                $user = $this->productUserTable->getByToken($hash);

                if ($user) {
                    $selfInvite = $this->productUserTable->getActivationExpiry($user->id);
                    $teamInvite = $this->productUserTable->getActivationExpiry($user->id);
                    $type = $user->invitation_type;

                    if ($type == '1') {
                        $tokenTime = strtotime($user->email_activation_time) + $teamInvite;

                        if ($tokenTime < time()) {
                            /** Token Expired */
                            /** Team Invite */

                            $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> The activation link has expired. <a href="/api/notify-admin-for-expired-invitation/?hash=' . $hash . '">Click here</a> if you would like to request a new activation link from your administrator.';
                            return ["message" => $message];
                        }
                    } elseif ($type == 2) {
                        $tokenTime = strtotime($user->email_activation_time) + $selfInvite;

                        if ($tokenTime < time()) {
                            /** Self Invite */

                            /** if Admin */
                            if ($user->invited_by) {
                                $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> The activation link has expired. <a href="/api/notify-admin-for-expired-invitation/?hash=' . $hash . '">Click here</a> if you would like to request a new activation link from your administrator.';
                            } else {
                                $message = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> To send another invitation, please login to the xxxxxxx portal.';
                            }

                            return ["message" => $message];
                        }
                    }

                    $clientOS = self::getOS($request->getServer()->get('HTTP_USER_AGENT'));

                    if ($clientOS == "Windows" || $clientOS == "MacOS") {
                        if ($clientOS == "Windows") {
                            $filename = "Setup_{$user->username}.exe";
                        } elseif ($clientOS == "MacOS") {
                            $filename = "MacOSSetup_{$user->username}.zip";
                        }

                        if (file_exists('/home/ftpuser/' . $user->username . '/' . $filename)) {
                            $filesize = filesize('/home/ftpuser/' . $user->username . '/' . $filename);
                            header("Content-Disposition: attachment; filename=$filename;");
                            header("Content-Type: application/octet-stream");
                            header('Content-Length: ' . $filesize);
                            readfile('/home/ftpuser/' . $user->username . '/' . $filename);
                            exit;
                        } else {

                            $message = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Installer not found, Please contact admin';
                            return ["message" => $message];
                        }

                    } elseif ($clientOS == "Android") {
                        header('Location: https://play.google.com/store/apps/details?id=com.xxxxxx.xxxx');
                        exit();
                    } elseif ($clientOS == "iPhone") {
                        header('Location: https://itunes.apple.com/us/genre/ios/id36?mt=8');
                    } else {
                        $message = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> You are using an unsupported device. xxxxx only supports the Windows, macOS, Android, or iOS operating systems. If you feel you are receiving this message in error, please contact our technical support.';
                        return ["message" => $message];
                    }
                } else {

                    /** No user found with this token */
                    $message = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> No user found with this token';
                    return ["message" => $message];
                }
            } else {

                $message = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Bad Request';
                return ["message" => $message];
            }
        } else {
            $data['status'] = 403;
            $data['message'] = 'Forbidden Access';
        }

        return new JsonModel($data);
    }


    /*
     * Send Activation link to client
     */
    function sendLinkAction()
    {
        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getQuery('key');
            $un = $request->getQuery('un');

            if ($key && $un) {

                $user = $this->productUserTable->getByUsernames($un);

                if ($user) {

                    $config = Module::getEnvConfig();
                    $selfInvite = $this->productUserTable->getActivationExpiry($user['id']);

                    /** Update email activation token */
                    //

                    $tokenTime = strtotime($user['email_activation_time']) + $selfInvite;

                    if ($tokenTime < time()) {
                        $user['email_activation_hash'] = md5($user['clientid'] . $user['productid'] . time());
                    }

                    $user['email_activation_time'] = new \Zend\Db\Sql\Expression("NOW()");
                    $user['invitation_type'] = 2;


                    $this->productUserTable->update($user);

                    /**
                     * Keep log in monolog
                     */

                    /** Send email with Download installer link */
                    $message_text = file_get_contents(__DIR__ . '/../Form/SendEmail.email');
                    $message_text = str_replace(array('$firstname', '$lastname', '$link', '$servername'), array($user['firstname'], $user['lastname'], $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user['email_activation_hash']), $_SERVER['SERVER_NAME']), $message_text);

                    $config = Module::getEnvConfig();

                    $html = new MimePart($message_text);
                    $html->type = "text/html";
                    $body = new MimeMessage();
                    $body->setParts(array($html));
                    $mail = new Mail\Message();

                    //Set mail body
                    $mail->setBody($body)
                        ->setFrom($config['fromEmail'], 'User Activation' . ' at' . ' xxxxx')
                        ->addTo($user['email'])
                        ->setSubject($config['EmailSubject']['bultInvite']);

                    $transport = new Mail\Transport\Smtp();
                    $options = new Mail\Transport\SmtpOptions(array(
                        'host' => $config['HOSTNAME'],
                        'port' => $config['PORT'],
                        'connection_class' => 'login',
                        'connection_config' => array(
                            'username' => $config['USERNAME'],
                            'password' => $config['PASSWORD'],
                            'ssl' => $config['SSL'],

                        ),
                    ));

                    $transport->setOptions($options);
                    $transport->send($mail);

                    $data['status'] = 200;
                    $data['message'] = 'Please check your email for the Activation link.';

                }
            }
        }
        die;
    }

    /**
     * Get NAS DATA
     */
    public function getNasDataAction()
    {
        $request = $this->getRequest();

        if ($request->isGet()) {
            $key = $request->getQuery('key');
            $nasIP = $request->getQuery('nasIP');
            $type = $request->getQuery('type');
            $hostname = $request->getQuery('hostname');

            if ($key && $nasIP && $type && $hostname) {


                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {

                    $nasSecret = $this->Nas->getNasByIp($nasIP);

                    if ($nasSecret) {
                        $data = '';
                        switch ($type) {
                            case 'secret' :
                                $data = $nasSecret->secret;
                                break;

                            case 'identifier' :
                                $data = $nasSecret->shortname;
                                break;

                            case 'country' :
                                $data = $nasSecret->country;
                                break;
                        }

                        echo $data;
                        exit;

                    } else {
                        switch ($type) {
                            case 'secret' :
                                $data = \Zend\Math\Rand::getString(28, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
                                break;

                            case 'identifier' :
                                $data = $hostname;
                                break;

                            case 'country' :
                                $data = file_get_contents('http://ipinfo.io/' . $nasIP . '/country');
                                break;
                        }

                        echo $data;
                        exit;
                    }

                }

            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }


        } else {
            $data = ['status' => 403];
        }

        return new JsonModel($data);
    }

    /**
     * Init Private
     * @description Clients will keep pinging it, it will update device used
     * @description Also shows a list of available servers
     */
    public function initPrivateAction()
    {

        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */


            $key = $request->getQuery('key');
            $id = $request->getQuery('id');
            $un = $request->getQuery('un');
            $device = $request->getQuery('device');
            $device_name = $request->getQuery('device_name');


            if ($key && $id && $un) {
                /**
                 * Update device
                 */

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    $user = (object)$this->productUserTable->getByRADUsername($un);

                    if ($user && $user->status == 'active') {

                        switch ($id) {
                            case 'win' :
                                $user->lastupdatewindows = new Expression('NOW()');
                                break;

                            case 'mac' :
                            case 'macos' :
                                $user->lastupdatemacos = new Expression('NOW()');
                                break;

                            case 'android' :
                                $user->lastupdateandroid = new Expression('NOW()');
                                break;

                            case 'ios' :
                                $user->lastupdateios = new Expression('NOW()');
                                break;
                        }
                        if ($id) {
                            if ($device) {
                                $this->UserDevices->findOrAdd($un, $id, base64_decode($device));
                            } else if ($device_name) {
                                $this->UserDevices->findOrAdd($un, $id, $device_name);
                            } else {
                                $this->UserDevices->findOrAdd($un, $id);
                            }
                        }

                        $this->productUserTable->update($user, false);

                        /**
                         * Figures out fastest server
                         */
                        $ipInfo = $this->location->getIP2Country();

                        if ($ipInfo->country_code == '-' OR $ipInfo->country_code == '') {
                            $ipInfo->country_code = 'US';
                        }

                        $countryCoords = $this->coords->getCountryByIso($ipInfo->country_code);

                        $userGroup = $this->groupsTable->getGroupDetails($user->fk_group_id);

                        if (($userGroup->group_fixed_ip_status == 'enabled') && ($userGroup->group_fixed_ip !== null)) {
                            $data['success'] = "true";
                            $data['servers'] = array();


                            $nases = $this->Nas->getNasData($userGroup->group_fixed_ip);
                            foreach ($nases as $nas) {

                                $country = $this->countriesTable->getCountryNameByIso($nas['country_name']);
                                $country->flag = 'https://xxxx.xxxx.com/images/iso/' . strtolower($nas['country_name']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['ISO'] = $nas['country_name'];
                                $nas['flag'] = $country->flag;
                                $nas['country_name'] = $country->name;
                                $nas['type'] = 'dedicated';


                                unset($nas['nasname']);
                                $data['servers'][] = (object)$nas;


                            }

                        } elseif ($userGroup->group_geo_settings == 'enabled') {

                            $data['success'] = "true";
                            $loc = $this->GroupLocationTable->getNasDataGeoCase($userGroup->group_id);

                            if ($userGroup->group_geo_all == 'enabled') {
                                $countryObj = $this->countriesTable->fetchAll();
                                foreach ($countryObj as $country) {
                                    $countrycode[] = $country->code;
                                }

                            } elseif ($userGroup->group_geo_specific == 'enabled') {
                                foreach ($loc as $lc) {
                                    $countryObj = $this->countriesTable->getCountryNameById($lc['fk_country_id']);
                                    $countrycode[] = $countryObj->code;
                                }

                            }

                            /**
                             * Country NASes
                             */
                            $closestNases = $this->Nas->getClosestNasesByCountryCode($countrycode);

                            foreach ($closestNases as $nas) {
                                $country = $this->countriesTable->getCountryNameByIso($nas['country_name']);
                                $countryFlag = 'https://xxxxx.xxxxx.com/images/iso/' . strtolower($nas['country_name']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['ISO'] = $nas['country_name'];
                                $nas['flag'] = $countryFlag;
                                $nas['country_name'] = $country->name;
                                $nas['type'] = 'geo';

                                unset($nas['nasname']);
                                $data['servers'][] = $nas;
                            }


                            $closestNases = $this->Nas->getClosestNases($countryCoords);

                            foreach ($closestNases as $nas) {

                                $country = $this->countriesTable->getCountryNameByIso($nas['ISO']);
                                $country->flag = 'https://xxxx.xxxxxx.com/images/iso/' . strtolower($nas['ISO']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['flag'] = $country->flag;
                                $nas['country_name'] = $country->name;
                                $nas['type'] = 'auto';

                                unset($nas['nasname']);
                                $data['servers'][] = $nas;


                            }

                        } else {
                            if ($countryCoords) {
                                $data['success'] = "true";

                                $closestNases = $this->Nas->getClosestNases($countryCoords);

                                foreach ($closestNases as $nas) {
                                    $country = $this->countriesTable->getCountryNameByIso($nas['ISO']);
                                    $country->flag = 'https://xxxx.xxxx.com/images/iso/' . strtolower($nas['CC']) . '.png';
                                    $nas['ip'] = $nas['nasname'];
                                    $nas['flag'] = $country->flag;
                                    $nas['country_name'] = $country->name;
                                    $nas['type'] = 'auto';


                                    unset($nas['nasname']);
                                    $data['servers'][] = $nas;


                                }
                            } else {
                                $data['status'] = 404;
                                $data['message'] = 'Country coordinate not found';
                            }
                        }


                    } else {
                        $data['status'] = 404;
                        $data['message'] = 'User not found';
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }
        }

        return new JsonModel($data);
    }


    /**
     * Init iOS
     * @description  Clients will keep pinging it, it will update device used
     * @description Also shows a list of available servers
     */
    public function pingIosAction()
    {
        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getQuery('key');
            $un = $request->getQuery('un');

            $device = $request->getQuery('device');


            if ($key && $un) {
                /**
                 * Update device
                 */

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {

                    $user = (object)$this->productUserTable->getByUsernames($un);

                    if ($user && $user->status == 'active') {

                        if ($device) {
                            $this->UserDevices->findOrAdd($un, 'ios', base64_decode($device));
                        }

                        $ipInfo = $this->location->getIP2Country();

                        if ($ipInfo->country_code == '-' OR $ipInfo->country_code == '') {
                            $ipInfo->country_code = 'US';
                        }

                        $countryCoords = $this->coords->getCountryByIso($ipInfo->country_code);

                        $user->lastupdateios = new Expression('NOW()');

                        $this->productUserTable->update($user, false);

                        $userGroup = $this->groupsTable->getGroupDetails($user->fk_group_id);

                        if (($userGroup->group_fixed_ip) && ($userGroup->group_fixed_ip_status == 'enabled')) {

                            $data['success'] = "true";


                            $nases = $this->Nas->getNasData($userGroup->group_fixed_ip);
                            foreach ($nases as $nas) {

                                $country = $this->countriesTable->getCountryNameByIso($nas['country_name']);
                                $country->flag = 'https://xxxxxx.xxxxxxx.com/images/iso/' . strtolower($nas['country_name']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['ISO'] = $nas['country_name'];
                                $nas['flag'] = $country->flag;
                                $nas['country_name'] = $country->name;
                                $nas['type'] = 'dedicated';
                                $nas['certs'] = 'https://' . $_SERVER['SERVER_NAME'] . '/api/ipsec-certs/?key=' . $this->apiToken . '&host=' . $user->group_fixed_ip;


                                unset($nas['nasname']);
                                $data['servers'][] = (object)$nas;


                            }

                        } else if ($userGroup->group_geo_settings == "enabled" && $userGroup->group_geo_specific == "enabled") {
                            $data['success'] = "true";

                            $group = $user->fk_group_id;

                            $countries_list = $this->GroupLocationTable->fetchAll($group);

                            $codes = [];
                            foreach ($countries_list as $single):
                                $codes[] = $single->code;
                            endforeach;

                            $closestNases = $this->Nas->getClosestNasesByCountryCode($codes);

                            foreach ($closestNases as $nas) {

                                $country = $this->countriesTable->getCountryNameByIso($nas['country_name']);

                                $country->flag = 'https://xxxx.xxxxxxx.com/images/iso/' . strtolower($nas['ISO']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['flag'] = $country->flag;
                                $nas['country_name'] = $country->name;
                                $nas['ISO'] = $nas['country_name'];//  $nas['type'] = 'Regular';
                                $nas['type'] = 'geo';                              //  $nas['type'] = 'Regular';
                                $nas['certs'] = $data['url'] = 'https://' . $_SERVER['SERVER_NAME'] . '/api/ipsec-certs/?key=' . $this->apiToken . '&host=' . $nas['nasname'];
                                unset($nas['nasname']);
                                $data['servers'][] = $nas;

                            }

                            $closestNases = $this->Nas->getClosestNases($countryCoords);

                            foreach ($closestNases as $nas) {
                                $country = $this->countriesTable->getCountryNameByIso($nas['ISO']);
                                $country->flag = 'https://xxxxxx.xxxxx.com/images/iso/' . strtolower($nas['ISO']) . '.png';
                                $nas['ip'] = $nas['nasname'];
                                $nas['flag'] = $country->flag;
                                $nas['country_name'] = $country->name;                              //  $nas['type'] = 'Regular';
                                $nas['type'] = 'auto';                              //  $nas['type'] = 'Regular';
                                $nas['certs'] = $data['url'] = 'https://' . $_SERVER['SERVER_NAME'] . '/api/ipsec-certs/?key=' . $this->apiToken . '&host=' . $nas['nasname'];
                                unset($nas['nasname']);
                                unset($nas['country']);
                                $data['servers'][] = $nas;

                            }

                        } else {
                            $ipInfo = $this->location->getIP2Country();

                            if ($ipInfo->country_code == '-' OR $ipInfo->country_code == '') {
                                $ipInfo->country_code = 'US';
                            }

                            $countryCoords = $this->coords->getCountryByIso($ipInfo->country_code);

                            if ($countryCoords) {
                                $data['success'] = "true";

                                $closestNases = $this->Nas->getClosestNases($countryCoords);

                                foreach ($closestNases as $nas) {

                                    $country = $this->countriesTable->getCountryNameByIso($nas['country']);

                                    $country->flag = 'https://xxxxxxxxx.xxxxxxxx.com/images/iso/' . strtolower($nas['ISO']) . '.png';
                                    $nas['ip'] = $nas['nasname'];
                                    $nas['flag'] = $country->flag;
                                    $nas['country_name'] = $country->name;                              //  $nas['type'] = 'Regular';
                                    $nas['type'] = 'auto';                              //  $nas['type'] = 'Regular';
                                    $nas['certs'] = $data['url'] = 'https://' . $_SERVER['SERVER_NAME'] . '/api/ipsec-certs/?key=' . $this->apiToken . '&host=' . $nas['nasname'];
                                    unset($nas['nasname']);
                                    $data['servers'][] = $nas;

                                }

                            } else {
                                $data['status'] = 404;
                                $data['message'] = 'Country coordinate not found';
                            }
                        }
                    } else {
                        $data['status'] = 404;
                        $data['message'] = 'User not found';
                    }
                }
            }
        }

        return new JsonModel($data);
    }


    /**
     * Download IPSec Certificates
     */
    public function ipsecCertsAction()
    {
        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getQuery('key');
            $host = $request->getQuery('host');

            if ($key && $host) {
                /**
                 * Update device
                 */

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    if (file_exists('/var/www/ipsec/' . $host . '/xxxx.p12')) {
                        header('Content-Type: application/octet-stream');
                        header("Content-Transfer-Encoding: Binary");
                        header("Content-disposition: attachment; filename=\"xxxxxxx.p12\"");
                        readfile('/var/www/ipsec/' . $host . '/xxxx.p12');
                        exit;
                    } else {
                        $data['status'] = 404;
                        $data['message'] = 'Certificate not available';
                    }
                }
            }
        }

        return new JsonModel($data);
    }


    /**
     * Send Invitation Link
     */
    public function addNasAction()
    {
        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getQuery('key');
            $nasname = $request->getQuery('nasname');
            $shortname = $request->getQuery('shortname');
            $secret = $request->getQuery('secret');
            $country = $request->getQuery('country', 'US');

            if ($key && $nasname && $shortname && $secret && $country) {

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    /** Delete existing NAS is there is any */
                    $this->Nas->deleteNasByName($nasname);

                    /** Create a New NAS */
                    $nasData = [];
                    $nasData['nasname'] = $nasname;
                    $nasData['shortname'] = $shortname;
                    $nasData['secret'] = $secret;
                    $nasData['description'] = 'xxxxxx Dev NAS';
                    $nasData['country'] = $country;

                    $boolAddNas = $this->Nas->addNas($nasData);

                    if ($boolAddNas) {
                        $data['status'] = 200;
                        $data['message'] = 'Success';
                    } else {
                        $data['status'] = 500;
                        $data['message'] = 'Error while adding NAS';
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }

            return new JsonModel($data);
        }
    }

    /**
     * Singup Action for Mobile and Desktop apps
     * @return JsonModel
     */
    public function signupAction()
    {
        $error = [];

        $i = 0;
        foreach (array('firstname', 'lastname', 'country', 'password', 'confirmpassword', 'email') as $item) {

            if (strlen(trim($this->getRequest()->getPost($item))) == 0) {
                if ($item == 'confirmpassword') {
                    $error[$i] = [$item => 'Confirm Password field is required'];

                } else {
                    $error[$i] = [$item => ucwords($item) . ' field is required'];

                }
                $i++;
            }
        }

        if ($this->getRequest()->getPost('email') != "") {
            $value = $this->getRequest()->getPost('email');
            $correct = preg_match("/^[0-9a-zA-Z\.\-\_\+]*?\@[0-9a-zA-Z][0-9a-zA-Z\.\-]*?\.[a-zA-Z]{2,}$/", $value);

            if (!$correct) {
                $item = 'email';

                $error[$i] = [$item => 'Incorrect email format'];
                $i++;
            }
            unset($value);
        }

        if ($this->getRequest()->getPost('password') != "" && $this->getRequest()->getPost('confirmpassword') != "") {
            if ($this->getRequest()->getPost('password') != $this->getRequest()->getPost('confirmpassword')) {
                $item = 'confirmpassword';

                $error[$i] = [$item => 'Confirm Password field is not match'];
                $i++;
            }
        }
        if (count($error) > 0) {
            $data = ["status" => "Error", "error" => $error, "data" => ""];
        } else {

            $pdata['firstname'] = $this->getRequest()->getPost('firstname');
            $pdata['lastname'] = $this->getRequest()->getPost('lastname');
            $pdata['email'] = $this->getRequest()->getPost('email');
            $pdata['companyname'] = $this->getRequest()->getPost('company');
            $pdata['password'] = $this->getRequest()->getPost('password');
            $pdata['country'] = $this->getRequest()->getPost('country');

            $client = ($this->createWHMCSclient($pdata));
            //Signup success working

            if ($client) {
                $order = $this->createWHMCSproduct($client, array('firstname' => $pdata['firstname'], 'lastname' => $pdata['lastname'], 'email' => $pdata['email'], 'role' => 'Full Admin'));

                if ($order) {

                    $this->updateUsernameAndPasswordWHMCS($client, $order->id, $pdata['password']);

                    $WHMCS = new CallWHMCS();
                    $dataWHMCS = json_decode($WHMCS->getResponse("getclientsproducts", ["serviceid" => $order->id], "json"));

                    $order = $dataWHMCS->products->product[0];

                    $product = $this->productUserTable->find_or_add($pdata['password'], $client, $order->id);
                    $userid = $product;
                    $id = $userid->id;
                    $product->username = $order->username;
                    $product->password = $order->password;
                    $product->role = 'Full Admin'; //'Trial User';
                    $product->status = "active";
                    $product->tfa = 'enabled';
                    $product_details['firstname'] = $pdata['firstname'];
                    $product_details['lastname'] = $pdata['lastname'];
                    $product_details['email'] = $pdata['email'];

                    $this->groupsTable->addDefault($id);

                    $this->productUserTable->update($product, true);
                    $this->productUsersProfileTable->update_or_add($product_details, $product->id);
                    $this->groupsTable->defaultGroup($id);

                    $message_text = file_get_contents(__DIR__ . '/../Form/NewProduct.email');
                    $message_text = str_replace(array('$firstname', '$lastname', '$link', '$servername', '$username', '$password'), array($pdata['firstname'], $pdata['lastname'], $_SERVER['SERVER_NAME'] . '/login', $_SERVER['SERVER_NAME'], $pdata['email'], $pdata['password']), $message_text);

                    $config = Module::getEnvConfig();
                    $html = new MimePart($message_text);
                    $html->type = "text/html";
                    $body = new MimeMessage();
                    $body->setParts(array($html));
                    $mail = new Mail\Message();
                    //Set mail body
                    $mail->setBody($body)
                        ->setFrom($config['fromEmail'], $config['fromName'])
                        ->addTo($pdata['email'])
                        ->setSubject($config['EmailSubject']['signup']);
                    $transport = new Mail\Transport\Smtp();
                    $options = new Mail\Transport\SmtpOptions(array(
                        'host' => $config['HOSTNAME'],
                        'port' => $config['PORT'],
                        'connection_class' => 'login',
                        'connection_config' => array(
                            'username' => $config['USERNAME'],
                            'password' => $config['PASSWORD'],
                            'ssl' => $config['SSL'],

                        ),
                    ));
                    $transport->setOptions($options);
                    $transport->send($mail);

                    $user = $this->productUserTable->getByUsername($id);

                    if ($user) {

                        $config = Module::getEnvConfig();

                        /** Update email activation token */

                        $selfInvite = $this->productUserTable->getActivationExpiry($user->id);

                        $tokenTime = strtotime($user->email_activation_time) + $selfInvite;

                        if ($tokenTime < time()) {
                            $user->email_activation_hash = md5($user->clientid . $user->productid . time());
                        }

                        $user->email_activation_time = new \Zend\Db\Sql\Expression("NOW()");
                        $user->invitation_type = 2;
                        $this->productUserTable->update($user, false);

                        /**
                         * Keep log in monolog
                         */


                        /** Send email with Download installer link */
                        $message_text = file_get_contents(__DIR__ . '/../Form/SendEmail.email');
                        $message_text = str_replace(array('$firstname', '$lastname', '$link', '$servername'), array($user->firstname, $user->lastname, $_SERVER['SERVER_NAME'] . '/api/download-installer?hash=' . urlencode($user->email_activation_hash), $_SERVER['SERVER_NAME']), $message_text);


                        $config = Module::getEnvConfig();
                        $html = new MimePart($message_text);
                        $html->type = "text/html";
                        $body = new MimeMessage();
                        $body->setParts(array($html));
                        $mail = new Mail\Message();
                        //Set mail body
                        $mail->setBody($body)
                            ->setFrom($config['fromEmail'], $config['fromName'])
                            ->addTo($user->email)
                            ->setSubject($config['EmailSubject']['ApiSendLink']);
                        $transport = new Mail\Transport\Smtp();
                        $options = new Mail\Transport\SmtpOptions(array(
                            'host' => $config['HOSTNAME'],
                            'port' => $config['PORT'],
                            'connection_class' => 'login',
                            'connection_config' => array(
                                'username' => $config['USERNAME'],
                                'password' => $config['PASSWORD'],
                                'ssl' => $config['SSL'],

                            ),
                        ));


                        $transport->setOptions($options);
                        $transport->send($mail);

                    }

                    $error = 'success';
                    $data = ["status" => "Success", "error" => $error, "data" => "ok"];
                } else {
                    $error = $_SERVER['DOCUMENT_ROOT'] . 'Error creating new product. Please contact system administrator';
                    $data = ["status" => "Error", "error" => $error, "data" => ""];
                }

            } else {
                $error = 'Error creating new customer. Please contact system administrator';
                $data = ["status" => "Error", "error" => $error, "data" => ""];
            }


        }
        return new JsonModel($data);
    }

    /**
     * Creates a WHMCS Client
     * @param $data
     * @return bool
     */
    public function createWHMCSclient($data)
    {
        $config = Module::getEnvConfig();
        $WHMCS = new CallWHMCS();

        $postfields = array();

        $postfields["action"] = "addclient";
        $postfields["firstname"] = $data["firstname"];
        $postfields["lastname"] = $data["lastname"];
        $postfields["email"] = $data["email"];
        $postfields["address1"] = 'Not Yet Set';
        $postfields["city"] = 'Not Yet Set';
        $postfields["state"] = 'Not Yet Set';
        $postfields["postcode"] = 'Not Yet Set';
        $postfields["country"] = $data["country"];
        $postfields["phonenumber"] = '00000000';
        $postfields["companyname"] = $data["companyname"];
        $postfields["password2"] = $data["password"];
        $postfields["noemail"] = "true";

        $json = ($WHMCS->getResponse("addclient", $postfields, "json"));

        if (!$json) {

            return false;
        } else {

            $arr = json_decode($json); # Parse XML

            if ($arr->result == "success") {
                return $arr->clientid;
            } else {
                return false;
            }
        }
    }

    /**
     * Update WHCMS Client username and password
     * @param $customerid
     * @param $productid
     * @param $newpass
     * @return array
     */
    public function updateUsernameAndPasswordWHMCS($customerid, $productid, $newpass)
    {

        $radiusUser = $this->ProcessUsername->getOne();

        $WHMCS = new CallWHMCS();
        $postfields = array();

        $postfields["serviceid"] = $productid;
        $postfields["serviceusername"] = $radiusUser->username;
        $postfields["servicepassword"] = mb_convert_encoding($newpass, 'UTF-8', 'ASCII');
        $json = $WHMCS->getResponse("updateclientproduct", $postfields, "json");

        if (!$json) {

            return array();
        } else {
            $radiusUser->status = 1;
            $this->ProcessUsername->update($radiusUser);

        }
    }

    /**
     * Add a WHMCS Product
     * @param $clientid
     * @param $product_data
     * @return bool
     */
    public function createWHMCSproduct($clientid, $product_data)
    {
        $WHMCS = new CallWHMCS();

        $dataWHMCS = json_decode($WHMCS->getResponse("getclients", ["search" => ''], "json"));

        $clients = $dataWHMCS->clients->client;

        foreach ($clients as $client) {

            $dataWHMCS = json_decode($WHMCS->getResponse("getclientsproducts", ["clientid" => $client->id], "json"));

            $products = $dataWHMCS->products->product;
            foreach ($products as $product) {
                $pid = $product->id;
            }
        }

        $dataWHMCS = json_decode($WHMCS->getResponse("getclientsproducts", ["serviceid" => $pid], "json"));
        $current_product_data = $dataWHMCS->products->product[0];

        foreach ($current_product_data->customfields->customfield as $custom_field) {
            if ($custom_field->name == 'First Name') {
                $field_tag['firstname'] = $custom_field->id;
            }
            if ($custom_field->name == 'Last Name') {
                $field_tag['lastname'] = $custom_field->id;
            }
            if ($custom_field->name == 'Email') {
                $field_tag->name = $custom_field->id;
            }
            if ($custom_field->name == 'Role') {
                $field_tag['role'] = $custom_field->id;
            }
        }

        $postfields = [];
        $postfields["clientid"] = $clientid;
        $postfields["pid"] = $current_product_data->pid;
        $postfields['customfields'] = base64_encode(serialize(array($field_tag['firstname'] => $product_data['firstname'], $field_tag['lastname'] => $product_data['lastname'], $field_tag['email'] => $product_data['email'], $field_tag['role'] => $product_data['role'])));

        $postfields["noemail"] = "true";
        $postfields["billingcycle"] = $current_product_data->billingcycle;
        $postfields["paymentmethod"] = "tco"; //$current_product_data['PRODUCT']['PAYMENTMETHOD'];

        $json = $WHMCS->getResponse("addorder", $postfields, "json");

        if (!$json) {

            return false;
        } else {

        }

        $product_arr = json_decode($json);
        if ($product_arr->result == 'error') {
            return false;
        }

        $postfields = array();
        $postfields["autosetup"] = true;
        $postfields["sendemail"] = false;
        $postfields["orderid"] = $product_arr->orderid;

        $dataWHMCS = $WHMCS->getResponse("acceptorder", $postfields, "json");

        $dataWHMCS = json_decode($WHMCS->getResponse("getclientsproducts", ["serviceid" => $product_arr->productids], "json"));


        $created_product = $dataWHMCS->products->product[0];
        $dataWHMCS = json_decode($WHMCS->getResponse("getclientsproducts", ["serviceid" => $created_product->id], "json"));

        return $dataWHMCS->products->product[0];
    }


    /**
     * Save IPSec certification from Shell Script
     */
    public function saveIpsecCertsAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * IPSec certs
             */
            $key = $request->getPost('key');
            $host = $request->getPost('host');
            $caCert = $request->getFiles('caCert');
            $p12 = $request->getFiles('p12');

            if ($key && $host && $caCert && $p12) {

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    $pathname = '/var/www/ipsec/' . $host;

                    @unlink($pathname . '/ca.pem');
                    @unlink($pathname . '/xxxxx.p12');
                    @rmdir($pathname);
                    if (mkdir($pathname, 0777)) {

                        move_uploaded_file($caCert['tmp_name'], $pathname . '/ca.pem');
                        move_uploaded_file($p12['tmp_name'], $pathname . '/xxxxxx.p12');

                        $data['status'] = 200;
                        $data['message'] = 'Success';
                    } else {
                        $data['status'] = 500;
                        $data['message'] = 'Error while saving certificates';
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }

            return new JsonModel($data);
        }
    }


    /**
     * Get Tickets List 2
     * @package WHMCS APIs
     */
    public function getTickets2Action()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $this->getRequest()->getPost('key');
            $username = $request->getPost('username');

            $startLimit = $request->getFiles('startLimit');
            $endLimit = $request->getFiles('endLimit');

            $startLimit = ($startLimit) ? $startLimit : 0;
            $endLimit = ($endLimit) ? $endLimit : 25;

            if ($key && $username) {
                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    /** start http request */
                    $WHMCS = new CallWHMCS();

                    $postfields = array();

                    $postfields["limitstart"] = $startLimit;
                    $postfields["limitnum"] = $endLimit;

                    $json = $WHMCS->getResponse("GetTickets", $postfields, "json");

                    if (!$json) {

                        return false;
                    } else {

                        $data = json_decode($json)->tickets->ticket;
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }

        }

        return new JsonModel($data);
    }

    /**
     * Send Team Invite
     * Send Invitation Link to team member from Admin email link
     */
    public function sendTeamInviteAction()
    {

        $request = $this->getRequest();

        if ($request->isGet()) {
            /**
             * @todo Update Client OS in Product Username Table
             */
            $hash = $request->getQuery('hash');

            if ($hash) {

                $user = $this->productUserTable->getByToken($hash);
                $masterAccount = $this->productUserTable->getByID($user->invited_by);
                if ($user && $user->role == 'User') {

                    /** Update email activation token */
                    $config = Module::getEnvConfig();
                    $teamInvite = $this->productUserTable->getActivationExpiry($user->id);
                    $tokenTime = strtotime($user->email_activation_time) + $teamInvite;

                    if ($tokenTime < time()) {
                        $user->email_activation_hash = md5($user->clientid . $user->productid . time());
                    }

                    $user->email_activation_time = new \Zend\Db\Sql\Expression("NOW()");
                    $user->invitation_type = 1;
                    $this->productUserTable->update($user, false);


                    /** Send email with Download installer link */
                    $message_text = file_get_contents(__DIR__ . '/../Form/sendTeamInvite.email');

                    $message_text = str_replace(array(
                        '$firstname',
                        '$lastname',
                        '$link',
                        '$invitername',
                        '$username',
                        '$password',
                        '$servername'
                    ), array(
                        $user->firstname,
                        $user->lastname,
                        $_SERVER['SERVER_NAME'] . '/api/download-installer/?hash=' . urlencode($user->email_activation_hash),
                        $masterAccount->firstname . ' ' . $masterAccount->lastname,
                        $user->username,
                        '',
                        $_SERVER['SERVER_NAME']
                    ), $message_text);

                    $config = Module::getEnvConfig();
                    $html = new MimePart($message_text);
                    $html->type = "text/html";
                    $body = new MimeMessage();
                    $body->setParts(array($html));
                    $mail = new Mail\Message();
                    $mail->setBody($body)
                        ->setFrom($config['fromEmail'], $config['fromName'])
                        ->addTo($user->email, $user->firstname . ' ' . $user->lastname)
                        ->setSubject($config['EmailSubject']['ApiSendLink']);
                    $transport = new Mail\Transport\Smtp();
                    $options = new Mail\Transport\SmtpOptions(array(
                        'host' => $config['HOSTNAME'],
                        'port' => $config['PORT'],
                        'connection_class' => 'login',
                        'connection_config' => array(
                            'username' => $config['USERNAME'],
                            'password' => $config['PASSWORD'],
                            'ssl' => $config['SSL'],

                        ),
                    ));

                    $transport->setOptions($options);
                    $transport->send($mail);

                    $message = ' <i class="fa fa-check-circle" aria-hidden="true"></i> A new invitation has been sent to ' . $user->firstname . ' ' . $user->lastname;

                } else {

                    $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> User not found';

                }

            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';

                $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Bad Request';
            }
        }

        return ["message" => $message];
    }


    /**
     * login and activate client
     */
    public function activateClientAction()
    {
        $request = $this->getRequest();

        $config = Module::getEnvConfig();

        if ($request->isGet()) {

            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = $request->getQuery('key');
            $email = $request->getQuery('email');

            if ($key && $email) {
                /**
                 * Update device
                 */

                if ($key != $this->apiToken) {
                    $data['status'] = 403;
                    $data['message'] = 'Forbidden Access';
                } else {
                    /** Find User by email */
                    $findByEmail = $this->productUserTable->checkEmail($email);

                    if ($findByEmail) {
                        $teamInvite = $this->productUserTable->getActivationExpiry($findByEmail->id);
                        $selfInvite = $this->productUserTable->getActivationExpiry($findByEmail->id);
                        $radiusUser = $this->RadCheckTable->getUserByUsername($findByEmail->username);
                        $type = $findByEmail->invitation_type;
                        if ($type == '1') {
                            $tokenTime = strtotime($findByEmail->email_activation_time) + $teamInvite;
                            $download = false;

                            if ($tokenTime < time()) {
                                /** Token Expired */
                                /** Team Invite */
                                $data['status'] = 403;
                                $data['message'] = 'The invitation has expired. Please ask your administrator for a new invitation.';
                            } else {
                                $download = true;
                            }
                        } elseif ($type == 2) {
                            $tokenTime = strtotime($findByEmail->email_activation_time) + $selfInvite;

                            if ($tokenTime < time()) {

                                /** Self Invite */
                                $data['status'] = 403;
                                $data['message'] = 'The invitation has expired. Please try activating another device to continue.';
                            } else {
                                $download = true;
                            }
                        }
                        if ($download) {
                            $keyConfig = $config['MobileKey'];

                            $blockCipher = BlockCipher::factory('mcrypt', array('algo' => 'aes'));
                            $blockCipher->setKey($keyConfig);
                            $encryptedKey = base64_encode($blockCipher->encrypt($findByEmail->id));

                            $data['status'] = 200;
                            $data['message'] = 'Activation Successful';
                            $data['url'] = 'https://' . $_SERVER['SERVER_NAME'] . '/api/ovpn-certs?key=' . ($encryptedKey);
                            $data['username'] = $radiusUser->username;
                            $data['password'] = $radiusUser->value;
                        }

                    } else {
                        $data['status'] = 404;
                        $data['message'] = 'That email address is invalid or the invitation has expired. Please try again.';
                    }
                }
            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }
        }

        return new JsonModel($data);
    }


    /**
     * Show OPVN assets json/zip for mobile Application
     * @return mixed
     */
    public function ovpnCertsAction()
    {
        $config = Module::getEnvConfig();
        $keyConfig = $config['MobileKey'];

        if (isset($_GET['key'])) {
            $encryptedUserName = base64_decode($_GET['key']);

            /** Private key saved on local */
            $keyConfig = $keyConfig = $config['MobileKey'];
            $blockCipher = BlockCipher::factory('mcrypt', array('algo' => 'aes'));
            $blockCipher->setKey($keyConfig);
            $id = $blockCipher->decrypt($encryptedUserName);

            $user = $this->productUserTable->getByID($id);
            if ($user) {
                if (isset($_GET['filename']) AND in_array($_GET['filename'], array('client.ovpn', 'client.key', 'client.crt', 'ca.crt', 'ta.key', 'login.conf'))) {
                    /**
                     *
                     * @todo OVPN credentials
                     */
                    /**
                     * @todo Fixed OVPN credential files
                     * @todo ca.crt and ta.key should be permanently fixed for all users
                     * @todo login.conf and client.ovpn are fixed temporarely, this supposed to be dynamic later
                     */
                    if (in_array($_GET['filename'], ['ca.crt', 'ta.key'])) {
                        $lowerName = strtolower($_GET['filename']);

                        $ovpnBaseDir = realpath(__DIR__ . '/../../../../ovpn_config');
                        if (file_exists($ovpnBaseDir . '/' . $lowerName)) {
                            header("Content-Type: application/octet-stream");
                            header("Content-Transfer-Encoding: Binary");
                            header("Content-disposition: attachment; filename=\"" . $lowerName . "\"");
                            echo file_get_contents($ovpnBaseDir . '/' . $lowerName);
                            die;
                        } else {
                            die('File not exists');
                        }
                    } else {
                        /**
                         * @todo Dynamic files, coming from whmcs/freeRadius database
                         */
                        $files = $this->getFreeradiusFiles($user->productid, $user->username);
                        if (array_key_exists(strtoupper($_GET['filename']), $files)) {
                            header("Content-Type: application/octet-stream");
                            header("Content-Transfer-Encoding: Binary");
                            header("Content-disposition: attachment; filename=\"" . $_GET['filename'] . "\"");
                            echo base64_decode($files[strtoupper($_GET['filename'])]);
                            die;
                        } else {
                            die('File not exists');
                        }
                    }
                } else {

                    /**
                     * Set response header for json
                     */
                    header('Content-Type: application/json');
                    $this->layout = false;

                    $jsonData = array();

                    $jsonData['CLIENT.OVPN'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('client.ovpn');
                    $jsonData['CLIENT.KEY'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('client.key');
                    $jsonData['CLIENT.CRT'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('client.crt');
                    $jsonData['CA.CRT'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('ca.crt');
                    $jsonData['LOGIN.CONF'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('login.conf');
                    $jsonData['TA.KEY'] = 'https://' . $_SERVER['HTTP_HOST'] . '/api/ovpn-certs?key=' . urlencode($_GET['key']) . '&filename=' . urlencode('ta.key');

                    echo json_encode($jsonData);
                    exit(0);

                }
            } else {
                header('HTTP/1.0 403 Forbidden');
                echo '<h2>Forbidden Access</h2>';
            }
        }
        die();
    }

    /**
     * Notify Admin For Expired Invitation
     * Notifiy master account about expired invitation event
     */
    public function notifyAdminForExpiredInvitationAction()
    {


        $request = $this->getRequest();

        $hash = $request->getQuery('hash');

        if ($hash) {
            $user = $this->productUserTable->getByToken($hash);
            $masterAccount = $this->productUserTable->getByID($user->invited_by);
            if ($user && $user->role == 'User') {


                $message_text = file_get_contents(__DIR__ . '/../Form/notifyAdminForExpiredInvitation.email');

                $message_text = str_replace(array(
                    '$firstname',
                    '$lastname',
                    '$invitername',
                    '$servername',
                    '$resendInvitationLink'
                ), array(
                    $masterAccount->firstname,
                    $masterAccount->lastname,
                    $user->firstname . ' ' . $user->lastname,
                    $_SERVER['SERVER_NAME'],
                    $_SERVER['SERVER_NAME'] . '/api/send-team-invite?hash=' . $hash
                ), $message_text);

                $config = Module::getEnvConfig();
                $html = new MimePart($message_text);
                $html->type = "text/html";
                $body = new MimeMessage();
                $body->setParts(array($html));
                $mail = new Mail\Message();
                $mail->setBody($body)
                    ->setFrom($config['fromEmail'], $config['fromName'])
                    ->addTo($masterAccount->email, $masterAccount->firstname . ' ' . $masterAccount->lastname)
                    ->setSubject($config['EmailSubject']['NotifyAdminForExpiredInvitation']);
                $transport = new Mail\Transport\Smtp();
                $options = new Mail\Transport\SmtpOptions(array(
                    'host' => $config['HOSTNAME'],
                    'port' => $config['PORT'],
                    'connection_class' => 'login',
                    'connection_config' => array(
                        'username' => $config['USERNAME'],
                        'password' => $config['PASSWORD'],
                        'ssl' => $config['SSL'],

                    ),
                ));


                $transport->setOptions($options);
                $transport->send($mail);

                $message = ' <i class="fa fa-check-circle" aria-hidden="true"></i> Your request for a reactivation link has been sent to your administrator..';

            } else {
                $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> You are not a valid User. Please go back and try again.';
            }
        } else {
            $message = ' <i class="fa fa-exclamation-circle" aria-hidden="true"></i> This is not a valid request, Please go back and try again. ';
        }

        return ["message" => $message];
    }

    /**
     * Add ticket
     * @package WHMCS APIs
     */
    public function addTicketAction()
    {
        $data = ["status" => '', 'message' => ''];
        $request = $this->getRequest();
        if ($request->isPost()) {

            /**
             * @todo Update Client OS in Product Username Table
             */
            $key = 1;
            $radUser = $request->getPost('username');
            $subject = $request->getPost('subject');
            $message = $request->getPost('message');
            $logFile = $request->getPost('logfile');

            if ($key && $radUser) {
                /** start http request */

                $WHMCS = new CallWHMCS();
                $user = (object)$this->productUserTable->getByUsernames($radUser);
                $postfields = array();
                $postfields["action"] = 'OpenTicket';
                $postfields["subject"] = $subject;
                $postfields["message"] = $message;
                $postfields["name"] = $user->firstname . ' ' . $user->lastname;
                $postfields["email"] = $user->email;
                $postfields["deptid"] = 1;
                $postfields["responsetype"] = "json";
                $json = $WHMCS->getResponse("OpenTicket", $postfields, "json");
                if (!$json) {
                    return false;
                } else {
                    $data = json_decode($json, true);
                    if ($logFile) {
                        $logFile = base64_decode($logFile);


                        $notepostfields = array();

                        $notepostfields["message"] = $logFile;
                        $notepostfields["ticketid"] = $data['id'];


                        $json = $WHMCS->getResponse("AddTicketNote", $notepostfields, "json");
                        $data = json_decode($json, 1);

                        if ((!$json || $data['result'] == 'error')) {

                            $data['status'] = '400';
                            $data['message'] = 'Error while attaching log file, Please try again';
                        } else {

                            $data['status'] = '200';
                            $data['message'] = 'Successfully ';

                        }

                    }
                    $param = ['key' => $key, 'username' => $radUser];

                    return $this->redirect()->toRoute('get-tickets-view', [], array('query' => $param));

                }

            } else {
                $data['status'] = 400;
                $data['message'] = 'Bad Request';
            }


        }
        $this->layout()->setTemplate('layout/newLayout');
        return ["data" => $data];
    }


    /**
     * @todo Helpers function
     */

    /**
     * get OS
     */
    public static function getOS($user_agent)
    {
        $os_platform = "Unknown OS Platform";
        $os_array = array(
            '/windows nt 10/i' => 'Windows',
            '/windows nt 6.3/i' => 'Windows',
            '/windows nt 6.2/i' => 'Windows',
            '/windows nt 6.1/i' => 'Windows',
            '/windows nt 6.0/i' => 'Windows',
            '/windows nt 5.2/i' => 'Windows',
            '/windows nt 5.1/i' => 'Windows',
            '/windows xp/i' => 'Windows',
            '/windows nt 5.0/i' => 'Windows',
            '/windows me/i' => 'Windows',
            '/win98/i' => 'Windows',
            '/win95/i' => 'Windows',
            '/win16/i' => 'Windows',
            '/macintosh|mac os x/i' => 'MacOS',
            '/mac_powerpc/i' => 'MacOS',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Linux',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPhone',
            '/ipad/i' => 'iPhone',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }

    /**
     * Get Free Radius OVPN Certs files
     * @param $userid
     * @param $service_username
     * @return array
     */
    public function getFreeradiusFiles($userid, $service_username)
    {
        $config = Module::getEnvConfig();
        $whmcsUrl = $config['whmcs_api']['url']; # URL to WHMCS API file goes here
        $username = $config['whmcs_api']['user']; # Admin username goes here
        $password = $config['whmcs_api']['password']; # Admin password goes here

        $postfields = array();
        $postfields["username"] = $username;
        $postfields["password"] = md5($password);
        $postfields["action"] = "freeradiusfiles";
        $postfields["service_id"] = $userid;
        $postfields["service_username"] = $service_username;
        $postfields["responsetype"] = "xml";
        $url = $whmcsUrl . 'includes/api.php';
        $query_string = "";
        foreach ($postfields AS $k => $v) $query_string .= "$k=" . urlencode($v) . "&";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $xml = curl_exec($ch);

        if (curl_error($ch) || !$xml) {
            curl_close($ch);
            return array();
        } else {
            curl_close($ch);
            $arr = $this->whmcsapi_xml_parser($xml); # Parse XML
            return $arr['FILES'];
        }
    }

    /**
     *  XML Parser for WHMCS API
     * @param $rawxml
     * @return mixed
     */
    function whmcsapi_xml_parser($rawxml)
    {
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $rawxml, $vals, $index);
        xml_parser_free($xml_parser);
        $params = array();
        $level = array();
        $alreadyused = array();
        $x = 0;
        foreach ($vals as $xml_elem) {
            if ($xml_elem['type'] == 'open') {
                if (in_array($xml_elem['tag'], $alreadyused)) {
                    $x++;
                    $xml_elem['tag'] = $xml_elem['tag'] . $x;
                }
                $level[$xml_elem['level']] = $xml_elem['tag'];
                $alreadyused[] = $xml_elem['tag'];
            }
            if ($xml_elem['type'] == 'complete') {
                $start_level = 1;
                $php_stmt = '$params';
                while ($start_level < $xml_elem['level']) {
                    $php_stmt .= '[$level[' . $start_level . ']]';
                    $start_level++;
                }
                $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
                @eval($php_stmt);
            }
        }
        return ($params['WHMCSAPI']);
    }


}