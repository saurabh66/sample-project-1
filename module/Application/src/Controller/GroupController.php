<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\ProductUsersProfile;
use Application\Module;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Application\Model\CountriesTable;
use Application\Model\ProductUsersTable;
use Application\Model\GroupLocationTable;
use Application\Model\ProductUsersProfileTable;
use Application\Model\GroupsTable;
use Application\Model\CategoryTable;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr4\Request;

/**
 * Class GroupController
 * @package Application\Controller
 */
class GroupController extends AbstractActionController
{
    /**
     * @var CountriesTable|null
     */
    private $countriesTable;
    /**
     * @var ProductUsersTable|null '
     */
    private $productUserTable;
    /**
     * @var ProductUsersProfileTable|null
     */
    private $productUsersProfileTable;
    /**
     * @var GroupsTable|null
     */
    private $groupsTable;
    /**
     * @var GroupLocationTable|null
     */
    private $groupLocation;

    /**
     * GroupController constructor.
     * @param CountriesTable|null $countriesTable
     * @param ProductUsersTable|null $productUserTable
     * @param ProductUsersProfileTable|null $productUsersProfileTable
     * @param GroupsTable|null $groupsTable
     * @param GroupLocationTable|null $groupLocation
     */
    public function __construct(CountriesTable $countriesTable = null, ProductUsersTable $productUserTable = null, ProductUsersProfileTable $productUsersProfileTable = null, GroupsTable $groupsTable = null, GroupLocationTable $groupLocation = null)
    {
        $this->countriesTable = $countriesTable;
        $this->productUserTable = $productUserTable;
        $this->productUsersProfileTable = $productUsersProfileTable;
        $this->groupsTable = $groupsTable;
        $this->groupLocation = $groupLocation;
    }

    /**
     * @return \Zend\Http\Response
     * Checking Sesssion log
     */
    public function check_login_after()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        if (!isset($_SESSION['userid'])) {
            return $this->redirect()->toRoute('login');
        } else {
            if ($this->productUserTable->getLoginCheck($_SESSION['userid'])) {
                return true;
            } else {
                return $this->redirect()->toRoute('login');
            }
        }

    }

    /**
     * @return array
     * Group Main Page
     */
    public function indexAction()
    {
        $this->check_login_after();
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
        $this->layout()->setVariable('currentUserData', $currentUserData);
        $this->layout()->setVariable('title', "Group");
        $data = $this->groupsTable->getGroupslist($_SESSION['userid']);
        $data1 = $this->groupsTable->getGroupslist($_SESSION['userid']);
        return ["data" => $data, "data1" => $data1];
    }

    public function Diffusers($usersObject, $teamIds)
    {
        $a = [];
        foreach ($usersObject as $u):
            $a[] = $u->id;
        endforeach;
        return array_diff($a, $teamIds);
    }

    /**
     * @return array|\Zend\Http\Response
     */
    public function edituserAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            if (isset($this->getRequest()->getPost()['save'])) {
                $updateGroupid = $this->getRequest()->getPost()['saveid'];
                $users = $this->productUserTable->fetchGroupByUser($id);
                $teams = count($this->getRequest()->getPost()['users']) ? $this->getRequest()->getPost()['users'] : [];
                $extraUsers = $this->Diffusers($users, $teams);
                $defaultResult = $this->groupsTable->defaultGroup($_SESSION['userid']);
                foreach ($extraUsers as $singleUser):
                    $this->productUserTable->updateGroup($singleUser, $defaultResult->group_id);
                endforeach;
                if (count($teams)) {
                    foreach ($teams as $team):
                        $this->productUserTable->updateGroup($team, $updateGroupid);
                    endforeach;
                }
                echo json_encode(["success" => "Done"]);
                die;
            }
            $sm = new \Zend\Session\SessionManager;
            $sm->start(TRUE);
            $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
            $this->layout()->setVariable('currentUserData', $currentUserData);
            $this->layout()->setVariable('title', "Edit User");
            $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
            $singleGroupDetail = $this->productUserTable->getDataGroupID($id);
            $defaultResult = $this->groupsTable->defaultGroup($_SESSION['userid']);
            $userList = ($this->productUserTable->getTeamMembers($_SESSION['userid'], $defaultResult->group_id));
            return ["data" => $data, "singleCurrentUser" => $currentUserData, "singleData" => $singleGroupDetail, "userlist" => $userList, "groupid" => $id, "defaultGroup" => $defaultResult->group_id];
        } else {
            return $this->redirect()->toRoute('groups');
        }
    }

    /**
     * Edit Description action
     * @return array
     */
    public function editdescAction()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        $this->layout()->setVariable('title', "Edit Group Info User");
        $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
        return ["data" => $data];
    }

    /**
     * Edit Settings Action
     * @return array|\Zend\Http\Response
     */
    public function editsettingAction()
    {
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            if (isset($this->getRequest()->getPost()['save'])) {
                $group_device_activation = isset($this->getRequest()->getPost()['group_device_activation']) ? "enabled" : "disabled";
                $group_web_activation = isset($this->getRequest()->getPost()['group_web_activation']) ? "enabled" : "disabled";
                $group_support_ticket = isset($this->getRequest()->getPost()['group_support_ticket']) ? "enabled" : "disabled";
                $groupId = $this->getRequest()->getPost()['saveid'];
                $this->groupsTable->updateEditSetting($groupId, $group_device_activation, $group_web_activation, $group_support_ticket);
            }
            $sm = new \Zend\Session\SessionManager;
            $sm->start(TRUE);
            $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
            $this->layout()->setVariable('currentUserData', $currentUserData);
            $this->layout()->setVariable('title', "Edit Settings");
            $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
            $groupData = $this->groupsTable->getGroupDetails($id);
            return ["data" => $data, "groupid" => $id, "groupData" => $groupData];
        } else {
            return $this->redirect()->toRoute('groups');
        }
    }

    /**
     * Fixed IP Edit
     * @return array|\Zend\Http\Response
     */
    public function editfixedipAction()
    {
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
        $this->layout()->setVariable('currentUserData', $currentUserData);
        $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
        $this->groupsTable->updateEditFixedIP($groupId, $group_seperate_fixed_id, $group_fixed_ip, $group_backup_fixed_ip, $group_fixed_ip_status, $ip_requested, $droplet_id);
        $groupData = $this->groupsTable->getGroupDetails($id);
        $this->layout()->setVariable('title', "Edit Dedicated IP");
        if (!empty($id)) {
            if (isset($this->getRequest()->getPost()['save']) && $this->getRequest()->getPost()['randcheck'] == $_SESSION['rand']) {
                $group_fixed_ip_status = isset($this->getRequest()->getPost()['group_fixed_ip_status']) ? "enabled" : "disabled";
                $groupId = $this->getRequest()->getPost()['saveid'];
                if (!$groupData->droplet_id) {

                    /**
                     * Make the call to digitalocean's droplet.
                     */
                    //initiate our Guzzle client for cURL request
                    $client = new Client([
                        'base_uri' => 'https://api.digitalocean.com/v2/',
                    ]);
                    //send a request to digitalocean
                    $response = $client->post('droplets', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer f6c19e713c7bbd8e37203a4fd73f9d5540a9dfbc94fd3f42fe105ae51643bf63'
                        ],
                        'json' => [
                            'name' => 'DedicatedIP' . $groupData->group_id,
                            'region' => 'nyc3',
                            'size' => 's-1vcpu-1gb',
                            'image' => '33559801',
                            'ssh_keys' => ["0b:f0:33:33:34:a5:1e:6f:1c:92:0f:61:24:17:dc:49"],
                            'backups' => false,
                            'ipv6' => false,
                            'user_data' => null,
                            'private_networking' => null,
                            'volumes' => null,
                            'tags' => [
                                'web', 'demo'
                            ]
                        ]
                    ]);

                    // Set ip_requested to true. If no IP in table but ip requested true, in cronjob make sure to run check
                    // on DO server and get the IP/Finish the process.
                    $ip_requested = true;
                    // Get the droplet_id to later ping for the droplet ip.
                    if ($response->getStatusCode() == 202) {
                        $responseData = json_decode($response->getBody(), false);
                        $droplet_id = $responseData->droplet->id;
                    }
                }

                $this->groupsTable->updateEditFixedIP($groupId, $group_seperate_fixed_id, $group_fixed_ip,
                    $group_backup_fixed_ip, $group_fixed_ip_status, $ip_requested, $droplet_id);
                //mailing to all this group id
                $this->mailSendFixedip($groupId);
                $groupData = $this->groupsTable->getGroupDetails($id);
                $droplet_id = $groupData->droplet_id;
                return ["data" => $data, "groupid" => $id, "groupData" => $groupData, 'status' => $group_fixed_ip_status,
                    'droplet_id' => $droplet_id];
            }

            $groupData = $this->groupsTable->getGroupDetails($id);
            $droplet_id = $groupData->droplet_id;
            return ["data" => $data, "groupid" => $id, "groupData" => $groupData, 'status' => $group_fixed_ip_status,
                'droplet_id' => $droplet_id];
        } else {
            return $this->redirect()->toRoute('groups');
        }
    }

    /**
     * Saved editFixedSavedAction due to changes
     * above in the code
     */
    public function editfixedipSAVEDAction()
    {
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            if (isset($this->getRequest()->getPost()['save'])) {
                if ($this->getRequest()->getPost()['fixed_id_address'] == "same_fixed_ip") {
                    $group_seperate_fixed_id = "disabled";
                    $group_fixed_ip = $this->getRequest()->getPost()['group_fixed_ip'];
                    $group_backup_fixed_ip = $this->getRequest()->getPost()['group_backup_fixed_ip'];

                } else if ($this->getRequest()->getPost()['fixed_id_address'] == "group_seperate_fixed_id") {
                    $group_seperate_fixed_id = "enabled";
                    $group_fixed_ip = "";
                    $group_backup_fixed_ip = "";
                }
                $group_fixed_ip_status = isset($this->getRequest()->getPost()['group_fixed_ip_status']) ? "enabled" : "disabled";
                $groupId = $this->getRequest()->getPost()['saveid'];
                $this->groupsTable->updateEditFixedIP($groupId, $group_seperate_fixed_id, $group_fixed_ip, $group_backup_fixed_ip, $group_fixed_ip_status);

            }

            $sm = new \Zend\Session\SessionManager;
            $sm->start(TRUE);
            $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
            $this->layout()->setVariable('currentUserData', $currentUserData);
            $this->layout()->setVariable('title', "Edit Fixed IP");
            $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
            $groupData = $this->groupsTable->getGroupDetails($id);
            return ["data" => $data, "groupid" => $id, "groupData" => $groupData];
        } else {
            return $this->redirect()->toRoute('groups');
        }
    }

    /**
     * @return array|\Zend\Http\Response
     * Edit GEO Setting
     */
    public function editgeoAction()
    {
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            if (isset($this->getRequest()->getPost()['save'])) {
                $groupId = $this->getRequest()->getPost()['saveid'];
                if ($this->getRequest()->getPost()['group_geo'] == "group_geo_all") {
                    //delete the all the records in geo_location table
                    $group_geo_specific = "disabled";
                    $group_geo_all = "enabled";
                } else if ($this->getRequest()->getPost()['group_geo'] == "group_geo_specific") {
                    //save the geo_location data in list
                    $this->groupLocation->deleteCountries($groupId);
                    $countries = ($this->getRequest()->getPost()['countriesValues'] != "") ? explode(",", $this->getRequest()->getPost()['countriesValues']) : [];
                    if (count($countries)) {
                        foreach ($countries as $team):
                            $this->groupLocation->insertCountries($team, $groupId);
                        endforeach;
                    }
                    $group_geo_specific = "enabled";
                    $group_geo_all = "disabled";
                }
                $group_geo_settings = isset($this->getRequest()->getPost()['group_geo_settings']) ? "enabled" : "disabled";
                $this->groupsTable->updateGeo($groupId, $group_geo_all, $group_geo_specific, $group_geo_settings);
            }
            $sm = new \Zend\Session\SessionManager;
            $sm->start(TRUE);
            $currentUserData = $this->productUserTable->getByUsername($_SESSION['userid']);
            $this->layout()->setVariable('currentUserData', $currentUserData);
            $this->layout()->setVariable('title', "Edit GEO");
            $data = $this->groupsTable->getGroupslists($_SESSION['userid']);
            $groupData = $this->groupsTable->getGroupDetails($id);
            $countriesList = $this->countriesTable->fetchAll();
            $geolocationList = $this->groupLocation->fetchAll($id);
            return ["data" => $data, "groupid" => $id, "groupData" => $groupData, "countriesList" => $countriesList, "countriesData" => $geolocationList];
        } else {
            return $this->redirect()->toRoute('groups');
        }
    }

    /**
     * @return \Zend\Http\Response
     * Delete Group
     */
    public function deleteAction()
    {
        $this->check_login_after();
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            $sm = new \Zend\Session\SessionManager;
            $sm->start(TRUE);
            $userid = $_SESSION['userid'];
            $users = $this->productUserTable->fetchGroupByUser($id);
            $this->groupsTable->deleteGroup($userid, $id);
            $defaultResult = $this->groupsTable->defaultGroup($userid);
            foreach ($users as $singleUser):
                $this->productUserTable->updateGroup($singleUser->id, $defaultResult->group_id);
            endforeach;
        }
        return $this->redirect()->toRoute('groups');
    }

    /**
     * @return \Zend\Http\Response
     * Checking Sesssion log
     */
    public function checkLoginAjax()
    {
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        if (!isset($_SESSION['userid'])) {
            echo json_encode(array('status' => 'Unauthrosied'));
            die;
        }
    }

    /**
     * Add Group
     */
    public function addgroupAction()
    {
        $this->checkLoginAjax();
        $sm = new \Zend\Session\SessionManager;
        $sm->start(TRUE);
        if ($this->check_login_after()) {
            $tag = array();
            $error = array();
            foreach (array('group_name') as $item) {
                if (strlen(trim($this->getRequest()->getPost()[$item])) == 0) {
                    $tag[] = $item;
                    $error[] = ucwords($item) . ' field is required';
                }
            }
            if (count($error) == 0) {
                extract($this->getRequest()->getPost());
                if ($this->groupsTable->checkGroup($_SESSION['userid'], $this->getRequest()->getPost()['group_name'])) {
                    $this->groupsTable->addNew($_SESSION['userid'], $this->getRequest()->getPost());
                    echo json_encode(array('status' => 'Success'));
                } else {
                    $error[] = "Group Already Use ";
                    echo json_encode(array('status' => 'Error', 'error' => $error));
                }

            } else {
                echo json_encode(array('status' => 'Error', 'tag' => $tag, 'error' => $error));
            }
        } else {
            $error[] = "Invalid  Access";
            echo json_encode(array('status' => 'Error', 'error' => $error));
        }
        exit;
    }

    /**
     * @return array
     * Edit Group
     */
    public function editgroupAction()
    {
        $this->check_login_after();
        $this->layout()->setVariable('title', "Edit Group Info");
        $id = ($this->params()->fromRoute("id"));
        if (!empty($id)) {
            $groupData = $this->groupsTable->getGroupDetails($id);
            return ["groupData" => $groupData];
        }
    }

    /**
     *
     */
    public function updateinfoAction()
    {
        $this->check_login_after();
        $this->checkLoginAjax();
        $groupName = $this->getRequest()->getPost()['groupName'];
        $groupDescription = $this->getRequest()->getPost()['groupDescription'];
        $groupId = $this->getRequest()->getPost()['groupId'];
        $this->groupsTable->updateInfo($groupName, $groupDescription, $groupId);
        echo json_encode(array('status' => "Success"));
        exit;
    }

    /**
     * @param $id
     * Mail send for fixed IP
     */

    function mailSendFixedip($id)
    {
        $this->check_login_after();

        $data = $this->groupsTable->getUserForDedicatedGroupID($id);

        if (count($data)):
            foreach ($data as $single):
                $message_text = file_get_contents(__DIR__ . '/../Form/sendmailfixedip.email');
                $message_text = str_replace(array('$firstname', '$lastname'), array($single['firstname'], $single['lastname']), $message_text);

                $config = Module::getEnvConfig();

                $html = new MimePart($message_text);
                $html->type = "text/html";
                $body = new MimeMessage();
                $body->setParts(array($html));
                $mail = new Mail\Message();

                $mail->setBody($body)
                    ->setFrom($config['fromEmail'], $config['fromName'])
                    ->addTo($single['email'])
                    ->setSubject($config['EmailSubject']['purchageSubject']);
                echo "gaurav";

                $transport = new Mail\Transport\Smtp();
                $options = new Mail\Transport\SmtpOptions(array(
                    'host' => $config['HOSTNAME'],
                    'port' => $config['PORT'],
                    'connection_class' => 'login',
                    'connection_config' => array(
                        'username' => $config['USERNAME'],
                        'password' => $config['PASSWORD'],
                        'ssl' => $config['SSL'],

                    ),
                ));

                $transport->setOptions($options);
                $transport->send($mail);

            endforeach;
        endif;
        die;
    }
}