Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.config.options.elements.center) {
      //Get ctx from string
      var ctx = chart.chart.ctx;

      //Get options from the center object in options
      var centerConfig = chart.config.options.elements.center;
      var fontStyle = centerConfig.fontStyle || 'Arial';
      var txt = centerConfig.text;
      var color = centerConfig.color || '#000';
      var sidePadding = centerConfig.sidePadding || 20;
      var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
      //Start with a base font of 30px
      ctx.font = "30px " + fontStyle;

      //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      var stringWidth = ctx.measureText(txt).width;
      var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      var widthRatio = elementWidth / stringWidth;
      var newFontSize = Math.floor(30 * widthRatio);
      var elementHeight = (chart.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      var fontSizeToUse = Math.min(newFontSize, elementHeight);

      //Set font settings to draw it correctly.
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      //ctx.font = fontSizeToUse+"px " + fontStyle;
      ctx.font = "bold 68px 'Open Sans', Helvetica, sans-serif";
      ctx.fillStyle = color;

      //Draw text in center
      ctx.fillText(txt, centerX, centerY);
    }
  }
});
/*
window.onload = function() {
  if(document.getElementById("usersChart")) {
    var usersChart = document.getElementById("usersChart").getContext("2d");
    var usersObj = {
      recentlyActive: 2,
      last24Hours: 10,
      last7Days: 4,
      total: function() {return this.recentlyActive + this.last24Hours + this.last7Days;}
    }
    window.myDoughnut = new Chart(usersChart, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [usersObj.recentlyActive, usersObj.last24Hours, usersObj.last7Days],
          backgroundColor: [
            "#2aaae2",
            "#f79421",
            "#079247",
          ],
          borderWidth: 0
        }],
        labels: [
          "Recently Active",
          "Active Last 24 Hrs",
          "Inactive Last 7 Days",
        ]
      },
      options: {
        events: [],
        elements: {
          center: {
            text: usersObj.total(),
            color: '#2aaae2',
          }
        },
        cutoutPercentage: 65,
        responsive: true,
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        animation: {
          animateScale: false,
          animateRotate: true
        },
        tooltips: {
          enabled: false
        }
      }
    });
  }

  if(document.getElementById("devicesChart")) {
    var devicesChart = document.getElementById("devicesChart").getContext("2d");
    var devicesObj = {
      recentlyActive: 9,
      last24Hours: 31,
      last7Days: 3,
      total: function() {return this.recentlyActive + this.last24Hours + this.last7Days;}
    }
    window.myDoughnut = new Chart(devicesChart, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [devicesObj.recentlyActive, devicesObj.last24Hours, devicesObj.last7Days],
          backgroundColor: [
            "#2aaae2",
            "#f79421",
            "#ec2227",
          ],
          borderWidth: 0
        }],
        labels: [
          devicesObj.recentlyActive + " Recently Active",
          devicesObj.last24Hours + " Active Last 24 Hrs",
          devicesObj.last7Days + " Inactive Last 7 Days",
        ]
      },
      options: {
        events: [],
        elements: {
          center: {
            text: devicesObj.total(),
            color: '#2aaae2',
          }
        },
        cutoutPercentage: 65,
        responsive: true,
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        animation: {
          animateScale: false,
          animateRotate: true
        },
        tooltips: {
          enabled: false,
        }
      }
    });
  }
};
*/