(function($){
  $('[data-toggle="tooltip"]').tooltip();

  //$('#popupContact').modal('show');
  //$('#popupBilling').modal('show');
  //$('#popupAlert').modal('show');
  //$('#popupAlertDays').modal('show');
  //$('#popupActivationRequests').modal('show');
  //$('#popupActivationPartly').modal('show');
  //$('#popupActivationFully').modal('show');
  //$('#popupProfile').modal('show');
  //$('#popupEditUser').modal('show');
  //$('#popupEditUserAuth').modal('show');
  $('.blue-select-wrapper select, .transparent-select-wrapper select').selectric();
  $('.groups-edit-users .selects-wrapper .select-single-wrapper select').selectric({
    disableOnMobile: false,
    nativeOnMobile: false
  });

  // Validation
  $(".user-form").each(function(){
    $(this).validate({
      errorPlacement: function (error, element) {
        element.closest('.field-wrapper').append(error);
      },
      messages: {
        supportSubject: {
          required: 'Please enter a subject'
        }
      }
    });
  });
  // /Validation

  // Humberger menu additional js
  $('#header-nav .navbar-toggle').on('click', function(){
    $(this).closest('#header').toggleClass('menu-opened');
  });
  // /Humberger menu

  // Mobile header dropdowns
  $('#header-nav .navbar-collapse .has-dropdown').on('click', function(e){
    e.preventDefault();
    if(window.matchMedia('(max-width: 767px)').matches) {
      $(this).find('.dropdown').slideToggle(300);
    }
  });
  // /Mobile header dropdowns

  // Datepicker
  $('input[name="daterange"]').daterangepicker({
    //parentEl: '.reports-user-activity .daterange-wrapper',
    buttonClasses: "btn-orange",
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
    }
  });
  $('input[name="daterange"]').val('Time Period');

  $('input[name="daterange"]').on('show.daterangepicker', function(ev, picker) {
    $(this).addClass('datepicker-opened');
  });
  $('input[name="daterange"]').on('hide.daterangepicker', function(ev, picker) {
    $(this).removeClass('datepicker-opened');
  });
  // /Datepicker

  // .groups-main interaction
  $('.groups-main-blocks-wrapper .groups-main-block-header .title-wrapper').on('click', function(){
    var $thisParent = $(this).closest('.groups-main-block');
    $thisParent.toggleClass('opened');
    $thisParent.find('.groups-main-block-body').slideToggle();
  });
  // /.groups-main interaction

  // move items in select interactions
  function moveItems(origin, dest) {
    var $selectricDest = $(dest).closest('.select-single-wrapper').find('.selectric-items ul');
    $(origin).find(':selected').appendTo(dest);
    $(origin).closest('.select-single-wrapper').find('.selectric-items .selected').appendTo($selectricDest);
    $(dest).find(':selected').prop("selected", false);
    $(origin).selectric('refresh');
    $(dest).selectric('refresh');
  }
  $('.groups-edit-users .selects-wrapper .select-btn-move .move-left').on('click', function () {
    moveItems('.selects-wrapper .select-single-wrapper .select-right', '.selects-wrapper .select-single-wrapper .select-left');
  });
  $('.groups-edit-users .selects-wrapper .select-btn-move .move-right').on('click', function () {
    moveItems('.selects-wrapper .select-single-wrapper .select-left', '.selects-wrapper .select-single-wrapper .select-right');
  });
  // /move items in select select interaction

  // .groups-edit-fixed-ip interaction
  $('.groups-edit-fixed-ip #onoffFixedIp').change(function() {
    $('.groups-edit-fixed-ip .radio-wrapper').toggleClass('enabled');
    if($(this).is(':checked')) {
      var $checkBox = $('.groups-edit-fixed-ip #sameFixedIp');
      $checkBox.prop("checked", true).change();
    } else {
      $('.groups-edit-fixed-ip .user-form input[type="radio"]').prop("checked", false).change();
    }
  });
  $('.groups-edit-fixed-ip .radio-wrapper input').change(function() {
    if($('.groups-edit-fixed-ip #sameFixedIp').is(':checked')) {
      $('.groups-edit-fixed-ip #sameFixedIpWrapper .radio-input-wrapper').slideDown();
    } else {
      $('.groups-edit-fixed-ip #sameFixedIpWrapper .radio-input-wrapper').slideUp();
    }
  });
  // /.groups-edit-fixed-ip interaction

  // .groups-edit-geo-settings interaction
  $('.groups-edit-geo-settings #onoffLocations').change(function() {
    $('.groups-edit-geo-settings .radio-wrapper').toggleClass('enabled');
    if($(this).is(':checked')) {
      var $checkBox = $('.groups-edit-geo-settings #allLocations');
      $checkBox.prop("checked", true).change();
    } else {
      $('.groups-edit-geo-settings .user-form input[type="radio"]').prop("checked", false).change();
    }
  });
  $('.groups-edit-geo-settings .radio-wrapper input').change(function() {
    if($('.groups-edit-geo-settings #specificLocation').is(':checked')) {
      $('.groups-edit-geo-settings #specificLocationWrapper .selects-wrapper').slideDown();
    } else {
      $('.groups-edit-geo-settings #specificLocationWrapper .selects-wrapper').slideUp();
    }
  });
  // /.groups-edit-geo-settings interaction


  // .advanced interaction
  $('.advanced .days-number .days-number-input-wrapper  span').on('click', function() {
    var $button = $(this);
    var oldValue = $button.parent().find("input").val();

    if ($button.text() == "+") {
      var newVal = parseFloat(oldValue) + 1;
    } else {
      // Don't allow decrementing below zero
      if (oldValue > 0) {
        var newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 0;
      }
    }
    $button.parent().find("input").val(newVal);
  });
  // /.advanced interaction


  // show/hide .new-message-popup on a support page
  $('.inner-page-header.new-message-added .new-message .btn-orange').on('click', function(){
    $(this).siblings('.new-message-popup').fadeToggle(300);
  });
  $('.inner-page-header.new-message-added .new-message-popup .btn-cancel').on('click', function(e){
    e.preventDefault();
    $(this).closest('.new-message-popup').fadeToggle(300);
  });
  // /show/hide .new-message-popup on a support page


  function scrollToBottom($element) {
    var elementHeight = $element[0].scrollHeight;
    $element.stop().animate({ scrollTop: elementHeight}, 0);
  }


  // apply new scrollbar to .chat-messages-container, scroll to bottom and show after it's scrolled
  $(window).on("load", function(){
    var chatContainer = ".support-tickets .chat-messages-container";
    var $mobileChatContainer = $(".support-mobile .support-mobile-chat-container");
    $(chatContainer).mCustomScrollbar({
      theme: "minimal-dark",
      callbacks:{
        onTotalScroll:function(){
          $(chatContainer).css({ opacity: 1 });
        }
      }
    });
    $(chatContainer).mCustomScrollbar('scrollTo','bottom',{
      scrollInertia: 0
    });

    // support-mobile
    if($mobileChatContainer.length) {
      scrollToBottom($mobileChatContainer);
      $mobileChatContainer.css({ opacity: 1 });
    }
  });
  // /scroll .chat-messages-container to bottom and show after it's scrolled


  // adding message to .chat-messages-container
  $('.support-tickets .support-chat .btn-send, .support-mobile-chat-form .btn-send').on('click', function(e){
    e.preventDefault();
    function getCurrentDate() {
      var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      var d = new Date();
      var hr = d.getHours();
      var min = d.getMinutes();
      if (min < 10) {
        min = "0" + min;
      }
      var date = d.getDate();
      var month = months[d.getMonth()];
      var year = d.getFullYear();
      var x = date + " " + month + " " + year + " " + hr + ":" + min;
      return x;
    }

    var chatContainer = ".support-tickets .chat-messages-container";
    var $mobileChatContainer = $(".support-mobile .support-mobile-chat-container");
    var $messageTextarea = $(this).closest('.user-form').find('#chatMessage');
    var $attachmentIcon = $('#chatFile+label [class^=icon-]');
    var messageText = $messageTextarea.val();
    var messageDate = getCurrentDate();
    var messageAuthor = 'Paul Rosenthal';


    if($mobileChatContainer.length) {
      $mobileChatContainer.append(messageHTML);
      scrollToBottom($mobileChatContainer);
      $messageTextarea.val('');
      $attachmentIcon.attr('class', 'icon-attachment');
    } else {
      $(chatContainer).find('.mCSB_container').append(messageHTML);
      $(chatContainer).mCustomScrollbar('scrollTo','bottom',{
        scrollInertia: 0
      });
      $messageTextarea.val('');
      $attachmentIcon.attr('class', 'icon-attachment');
    }
  });
  // /adding message to .chat-messages-container

  // change icon on file upload
  $(".support-tickets #chatFile, .support-mobile-chat-form #chatFile").change(function(){
    $(this).siblings('.chat-file-label').find('[class^="icon-"]').attr('class', 'icon-check');
  });
  // /change icon on file upload
})(jQuery)
